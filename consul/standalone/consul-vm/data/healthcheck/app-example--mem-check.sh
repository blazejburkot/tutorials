#!/usr/bin/env bash

env > /vagrant/consul-vm/consul-service-healthcheck/env-check.log

mem_used=$(curl --request GET http://192.168.0.10:8080/actuator/metrics/jvm.memory.used --silent --fail | jq '.measurements[0].value')
mem_max=$(curl --request GET http://192.168.0.10:8080/actuator/metrics/jvm.memory.max --silent --fail | jq '.measurements[0].value')
mem_usage=$((mem_used * 100 / mem_max))

printf "Used: ${mem_used} bytes \nMax:  ${mem_max} bytes \nUsage: ${mem_usage} %% \n"

if [[ ${mem_usage} > 85 ]]; then
    echo "Service is using more than 85% of memory"
    exit 2
elif [[ ${mem_usage} > 70 ]]; then
    echo "Service is using more than 70% of memory"
    exit 1
fi
