#!/usr/bin/env bash

echo "Creating Consul Admin Token ... "
consul acl bootstrap &> /vagrant/consul-vm/acl-bootstrap.log

consul_secret_id=$(awk '/SecretID/{print $2}' /vagrant/consul-vm/acl-bootstrap.log)
echo "CONSUL_HTTP_TOKEN=${consul_secret_id}" | sudo tee -a /etc/environment
source /etc/environment

echo "RUN export CONSUL_HTTP_TOKEN=${consul_secret_id}"
export CONSUL_HTTP_TOKEN=${consul_secret_id}

echo "Creating Policy For Consul Configurer ... "
consul acl policy create -name "configurer-policy" \
  -description "Policy that grants unlimited access to ACL and KV" \
  -rules @/vagrant/consul-vm/config/policy/consul-configurer.hcl
consul acl token create \
  -description "[Admin] Consul configurer" \
  -policy-name "configurer-policy" &> /vagrant/consul-vm/acl-configurer.log
