#!/usr/bin/env bash

consul_secret_id=$(awk '/SecretID/{print $2}' /vagrant/consul-vm/acl-bootstrap.log)

# See service.id -> curl http://172.20.20.10:8500/v1/agent/services | jq .
curl --request PUT \
  -H "X-Consul-Token: ${consul_secret_id}" \
  --data @/vagrant/consul-vm/data/healthcheck/app-example--mem-check-def.json \
  http://172.20.20.10/v1/agent/check/register

echo "Done"