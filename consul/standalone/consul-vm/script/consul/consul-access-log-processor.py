#!/usr/bin/python3

# -*- coding: utf-8 -*-

# pip install python-consul
# pip install python-dateutil

import argparse
import json
import logging
from datetime import datetime
from typing import Any, TextIO
from urllib import parse

import consul
from consul.base import CB
import dateutil.parser

HTML_PAGE_START = '''<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>$PAGE_TITLE</title>
    <style type="text/css">
table { border-collapse: collapse; border-spacing: 0;}
table th { border-color: black; border-style: solid; border-width: 1px; font-family: Arial, sans-serif; font-size: 14px; font-weight: bold; overflow: hidden; padding: 5px 5px; word-break: normal; background-color: cornsilk;}
table td { border-color: black; border-style: solid; border-width: 1px; font-family: Arial, sans-serif; font-size: 14px; overflow: hidden; padding: 5px 5px; word-break: normal; }
table td, table th { text-align: left; vertical-align: top }
    </style>
</head>
<body>
<table class="tg">
    <h1>$PAGE_TITLE</h1>
    <p>Created at: $CREATION_TIME</p>
    <thead>
    <tr>
        <th>Time</th>
        <th>Http Method</th>
        <th>Http Path</th>
        <th>Status</th>
        <th>Client Address</th>
        <th>Auth Token Owner</th>
        <th>Http Referer</th>
    </tr>
    </thead>
    <tbody>
'''

HTML_PAGE_END = '''
    </tbody>
</table>
</body>
</html>
'''


def configure_logger():
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(levelname)-5s][%(asctime)s] - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')


def format_datetime(dt: datetime):
    return dt.strftime('%Y-%m-%d %H:%M:%S')


def get_uri_param(uri, param_name):
    query_str = parse.urlparse(uri).query
    params = parse.parse_qs(query_str)
    return dict_get(params, param_name, [''])[0]


def dict_get(obj: dict, param_name: str, default_value: Any = ''):
    return obj.get(param_name) if param_name in obj else default_value


def load_consul_tokens(host, port, auth_token, scheme):
    if len(auth_token) == 0 or len(host) == 0:
        return {}

    server = consul.Consul(host=host, port=port, scheme=scheme, verify=True, cert=None)
    tokens_raw = server.http.get(CB.json(), '/v1/acl/tokens', params=[('token', auth_token)])

    res = {}
    for token_raw in tokens_raw:
        token = server.http.get(CB.json(), f"/v1/acl/token/{token_raw['AccessorID']}", params=[('token', auth_token)])
        res[token['SecretID']] = token['Description']

    return res


def parse_log_data(consul_tokens, json_log) -> dict:
    log_raw_data = json.loads(json_log)
    client_raw_data = log_raw_data['client']
    req_raw_data = log_raw_data['request']
    req_uri = dict_get(req_raw_data, 'uri')
    req_path = parse.urlparse(req_uri).path
    header_token = dict_get(req_raw_data, 'header_consul_token')
    auth_token = header_token if len(header_token) > 0 else get_uri_param(req_uri, 'token')

    return {
        'client_addr': f"{dict_get(client_raw_data, 'addr')}:{dict_get(client_raw_data, 'port')}",
        'http_referrer': dict_get(req_raw_data, 'http_referrer'),
        'http_method': req_raw_data['method'],
        'req_id': req_raw_data['id'],
        'status': dict_get(log_raw_data['response'], 'status'),
        'req_uri': req_path,
        # 'auth_token': auth_token,
        'token_owner': dict_get(consul_tokens, auth_token),
        'time': dateutil.parser.parse(log_raw_data['time'])
    }


def start_html(file: TextIO, title: str):
    page_start = HTML_PAGE_START \
        .replace('$PAGE_TITLE', title) \
        .replace('$CREATION_TIME', format_datetime(datetime.now()))
    file.write(page_start)


def end_html(file: TextIO):
    file.write(HTML_PAGE_END)


def write_html_row(file: TextIO, data: dict):
    html_row = f'''        <tr>
            <td>{format_datetime(data['time'])}</td>
            <td>{data['http_method']}</td>
            <td>{data['req_uri']}</td>
            <td>{data['status']}</td>
            <td>{data['client_addr']}</td>
            <td>{data['token_owner']}</td>
            <td>{data['http_referrer']}</td>
        </tr>
'''
    file.write(html_row)


def process_access_log_file(args):
    consul_tokens = load_consul_tokens(host=args.host, port=args.port, auth_token=args.token, scheme=args.scheme)

    with open(args.access_log_path, 'r') as input_file, \
            open(args.kv_audit, 'w+') as kv_file, \
            open(args.acl_log, 'w+') as acl_file, \
            open(args.consul_access_log, 'w+') as access_log_file:
        start_html(kv_file, 'Consul KeyValue Audit')
        start_html(acl_file, 'Consul ACL Audit')
        start_html(access_log_file, 'Consul Access log')

        for json_log in input_file:
            if len(json_log.strip()) == 0:
                continue
            log_data = parse_log_data(consul_tokens, json_log)
            req_uri = log_data['req_uri']

            if log_data['http_method'] == 'GET':
                write_html_row(access_log_file, log_data)
            elif req_uri.startswith('/v1/kv/'):
                write_html_row(kv_file, log_data)
            elif req_uri.startswith('/v1/acl/'):
                write_html_row(acl_file, log_data)
            else:
                logging.error(f"Unknown request type {log_data['http_method']} {req_uri} {log_data}")

        end_html(kv_file)
        end_html(acl_file)
        end_html(access_log_file)
    logging.info("DONE")


def parse_arguments():
    parser = argparse.ArgumentParser(description='Consul API Access log processor')
    parser.add_argument('--host', action='store', default='', help='Consul host')
    parser.add_argument('--port', action='store', default='', help='Consul port')
    parser.add_argument('--scheme', action='store', default='http', help='Consul scheme')
    parser.add_argument('--token', action='store', default='', help='Consul Token')

    parser.add_argument('--access-log-path', action='store', default='/var/log/nginx/consul-api-access.log',
                        help='Path to access log')
    parser.add_argument('--kv-audit', action='store', default='/var/www/consul-audit/audit/kv-log.html',
                        help='Consul KeyValue Audit output file location')
    parser.add_argument('--acl-log', action='store', default='/var/www/consul-audit/audit/acl-log.html',
                        help='Consul ACL Audit output file location')
    parser.add_argument('--consul-access-log', action='store', default='/var/www/consul-audit/audit/access-log.html',
                        help='Consul Access Log output file location')

    args = parser.parse_args()
    logging.info('Arguments: %s', args)

    return args


if __name__ == "__main__":
    configure_logger()
    cmd_args = parse_arguments()
    process_access_log_file(cmd_args)
