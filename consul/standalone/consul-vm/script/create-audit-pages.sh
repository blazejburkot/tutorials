#!/usr/bin/env bash

consul_secret_id=$(awk '/SecretID/{print $2}' /vagrant/consul-vm/acl-bootstrap.log)

python3 /vagrant/consul-vm/script/consul/consul-access-log-processor.py \
  --host 127.0.0.1 \
  --port 8500 \
  --token ${consul_secret_id} &>> consul-api-access-log-processor.log

echo "" > /var/log/nginx/consul-api-access.log