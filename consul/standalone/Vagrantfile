# -*- mode: ruby -*-
# vi: set ft=ruby :

$install_consul = <<SCRIPT
    echo "Installing dependencies ..."
    sudo apt-get update
    sudo apt-get install -y unzip curl jq dnsutils python

    echo "Fetching Consul version ..."
    export CONSUL_VERSION=1.7.2
    cd /tmp/
    curl -s https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip -o consul.zip

    echo "Installing Consul ..."
    unzip consul.zip
    sudo chmod +x consul
    sudo mv consul /usr/bin/consul
    sudo mkdir /etc/consul.d
    sudo chmod a+w /etc/consul.d

    echo "Creating Consul Deamon ..."
    sudo mkdir -p /consul/{data,config,ui}
    sudo chmod -R a+w /consul
    sudo cp /vagrant/consul-vm/config/consul.service /etc/systemd/system/consul.service

    echo "Starting Consul Deamon ..."
    sudo systemctl daemon-reload
    sudo systemctl start consul
    sudo systemctl enable consul


    echo "Waiting 10 seconds ..."
    sleep 10

    echo "Configuring Consul ACL ..."
    /vagrant/consul-vm/script/configure-acl.sh

    echo "Installing Nginx ..."
    sudo apt-get install -y nginx

    sudo rm /etc/nginx/sites-enabled/default
    sudo cp /vagrant/consul-vm/config/nginx/nginx.conf /etc/nginx/nginx.conf
    sudo cp /vagrant/consul-vm/config/nginx/consul.conf /etc/nginx/conf.d/

    sudo mkdir -p /var/www/consul-audit/audit
    echo "Consul KV Audit Log" > /var/www/consul-audit/audit/kv-log.html
    echo "Consul ACL Audit Log" > /var/www/consul-audit/audit/acl-log.html
    echo "Consul Access Log" > /var/www/consul-audit/audit/access-log.html

    # Restarting Nginx ...
    sudo systemctl restart nginx

    # These libs are needed to run create-audit-pages.sh
    sudo apt-get install -y python3-pip
    sudo pip3 install python-consul
    sudo pip3 install python-dateutil


    # sudo netstat -tulpn | grep LISTEN

    echo "Done"
SCRIPT


$run_consul_configurer = <<SCRIPT
    echo "Installing python ..."
    sudo apt update
    sudo apt-get install -y unzip curl jq dnsutils python
    sudo apt-get install -y python3-pip

    echo "Installing python's libs ..."
    sudo pip3 install python-consul
    sudo pip3 install jproperties
    sudo pip3 install pyyaml
    sudo pip3 install -U pyyaml
    sudo pip3 install python-dateutil

    echo "Running configurer"
    cd /vagrant/configurer-vm
    sudo /vagrant/configurer-vm/script/run-configurer-scripts.sh

    echo "Done"
SCRIPT


Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu/bionic64"

    config.vm.define "consul" do |consul|
        consul.vm.base_mac = "080027D17374"

        consul.vm.hostname = "consul1.consul"
        consul.vm.network :private_network, ip: "172.20.20.10"
        consul.ssh.forward_agent = true

        consul.vm.provision "shell", inline: $install_consul

        consul.vm.provider "virtualbox" do |v|
            v.name = "consul-node1"
            v.customize ["modifyvm", :id, "--memory", "2048"]
            v.customize ["modifyvm", :id, "--ioapic", "on"]
            v.customize ["modifyvm", :id, "--cpus", "2"]
            v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        end

        consul.vm.provision :hosts do |provisioner|
            provisioner.sync_hosts = false
            provisioner.add_host '172.20.20.10', ['consul1.consul']
        end
    end

    config.vm.define "configurer" do |configurer|
        configurer.vm.hostname = "configurer.consul"

        configurer.vm.network :private_network, ip: "172.20.20.20"
        configurer.ssh.forward_agent = true

        configurer.vm.provision "shell", inline: $run_consul_configurer

        configurer.vm.provider "virtualbox" do |v|
            v.name = "consul-configurer"
            v.customize ["modifyvm", :id, "--memory", "1024"]
            v.customize ["modifyvm", :id, "--ioapic", "on"]
            v.customize ["modifyvm", :id, "--cpus", "1"]
            v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        end

        configurer.vm.provision :hosts do |provisioner|
            provisioner.sync_hosts = false
            provisioner.add_host '172.20.20.20', ['configurer.consul']
        end
    end
end
