# This is required to enable application to find other application which are register in Consul

service_prefix "" {
    policy = "read"
}

node_prefix "" {
  policy = "read"
}
