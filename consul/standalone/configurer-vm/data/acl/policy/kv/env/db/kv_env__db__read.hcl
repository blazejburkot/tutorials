# This policy enable to read 'backed-db' properties

key_prefix "env/db" {
    policy = "read"
}

key_prefix "secret/db" {
    policy = "read"
}
