#!/usr/bin/python3

# -*- coding: utf-8 -*-

# pip install python-consul
# pip install jproperties
# pip install pyyaml

import argparse
import glob
import json
import logging
import os
import re
from datetime import datetime
import yaml
import consul
from consul.base import CB

POLICY_OK = []
POLICY_CREATED = []
POLICY_UPDATED = []
POLICY_DELETED = []

ROLE_OK = []
ROLE_CREATED = []
ROLE_UPDATED = []
ROLE_DELETED = []

TOKEN_OK = []
TOKEN_CREATED = []
TOKEN_UPDATED = []
TOKEN_DELETED = []


def configure_logger():
    log_format = '[%(levelname)-5s][%(asctime)s] - %(message)s'
    log_date_format = '%Y-%m-%d %H:%M:%S'
    log_filename = f'logs/consul-acl-configurer_{datetime.now().strftime("%Y-%m-%d_%H-%M")}.log'

    formatter = logging.Formatter(log_format, datefmt=log_date_format)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    os.makedirs(os.path.dirname(log_filename), exist_ok=True)
    logging.basicConfig(filename=log_filename, level=logging.DEBUG, format=log_format, datefmt=log_date_format)
    logging.root.addHandler(console_handler)


def read_file(file_path):
    with open(file_path, 'r') as file:
        return file.read()


def read_yaml_file(file_path):
    with open(file_path) as file:
        return yaml.load(file, Loader=yaml.FullLoader)


def are_list_equal(list1: list, list2: list) -> bool:
    if (list1 is not None) and (list2 is not None):
        list1.sort()
        list2.sort()
        return list1 == list2
    else:
        return (list1 is None) == (list2 is None)


class ConsulAclClient:
    POLICY_NAME_REGEX = re.compile('^[\\w\\d_-]{1,128}$')
    ROLE_NAME_REGEX = re.compile('^[\\w\\d_-]{1,128}$')

    def __init__(self, args):
        self.__server__ = consul.Consul(host=args.host, port=args.port, scheme=args.scheme, verify=True, cert=None)
        self.__token__ = args.token
        self.__http__ = self.__server__.http
        self.__auth_param__ = [('token', args.token)]
        self.__readonly__ = args.readonly

    def list_policies(self):
        all_policies = self.__http__.get(CB.json(), '/v1/acl/policies', params=self.__auth_param__)
        return {el['Name']: el['ID'] for el in all_policies}

    def create_policy(self, name, rules: str):
        assert self.POLICY_NAME_REGEX.match(name), f"Illegal Policy name '{name}'"
        logging.info("[Policy] Creating '%s'\n   rules: %s", name, repr(rules))
        POLICY_CREATED.append(name)
        data = json.dumps({'Name': name, 'Rules': rules})
        if not self.__readonly__:
            self.__http__.put(CB.json(), '/v1/acl/policy', data=data, params=self.__auth_param__)

    def get_policy(self, policy_id):
        return self.__http__.get(CB.json(), f'/v1/acl/policy/{policy_id}', params=self.__auth_param__)

    def update_policy_rules(self, policy, policy_rules):
        POLICY_UPDATED.append(policy['Name'])
        logging.info("[Policy] Updating '%s'\n   before: %s\n    after: %s",
                     policy['Name'], repr(policy['Rules']), repr(policy_rules))
        data = json.dumps({'ID': policy['ID'], 'Name': policy['Name'], 'Rules': policy_rules})
        if not self.__readonly__:
            self.__http__.put(CB.json(), '/v1/acl/policy/' + policy['ID'], data=data, params=self.__auth_param__)

    def delete_policy(self, policy_id, policy_name):
        POLICY_DELETED.append(policy_name)
        logging.info(f"[Policy] Deleting name: `{policy_name}`, id: {policy_id}")
        if not self.__readonly__:
            self.__http__.delete(CB.json(), f'/v1/acl/policy/{policy_id}', params=self.__auth_param__)

    def list_roles(self):
        roles = self.__http__.get(CB.json(), '/v1/acl/roles', params=self.__auth_param__)
        return {role['Name']: role for role in roles}

    def create_role(self, role):
        role_name = role['Name']
        assert self.ROLE_NAME_REGEX.match(role_name), f"Illegal Role name '{role_name}'"
        logging.info(f"[Role] Creating '{role_name}', def: {role}")
        ROLE_CREATED.append(role_name)
        data = json.dumps({'Name': role['Name'], 'Description': role['Description'],
                           'Policies': [{'Name': el} for el in role['Policies']]})
        if not self.__readonly__:
            self.__http__.put(CB.json(), '/v1/acl/role', data=data, params=self.__auth_param__)

    def update_role(self, consul_role, role):
        role_id = consul_role['ID']
        role_name = role['Name']
        logging.info(f"[Role] Updating '{role_name}', ID: {role_id}\n   before: {{'Description': "
                     f"'{consul_role['Description']}', 'Policies': {get_role_policies_names(consul_role)}}}"
                     f"\n    after: {{'Description': '{role['Description']}', 'Policies': {role['Policies']}}}")
        ROLE_UPDATED.append(role_name)
        data = json.dumps({'Name': role['Name'], 'Description': role['Description'],
                           'Policies': [{'Name': el} for el in role['Policies']]})
        if not self.__readonly__:
            self.__http__.put(CB.json(), f'/v1/acl/role/{role_id}', data=data, params=self.__auth_param__)

    def delete_role(self, role):
        role_id = role['ID']
        role_name = role['Name']
        ROLE_DELETED.append(role_name)
        logging.info(f"[Role] Deleting name: `{role_name}`, id: {role_id}, def {role}")
        if not self.__readonly__:
            self.__http__.delete(CB.json(), f'/v1/acl/role/{role_id}', params=self.__auth_param__)

    def list_tokens(self):
        tokens = self.__http__.get(CB.json(), '/v1/acl/tokens', params=self.__auth_param__)
        return {token['Description']: token for token in tokens}

    def create_token(self, token):
        logging.info(f"[Token] Creating token {token}")
        TOKEN_CREATED.append(token['Description'])
        data = json.dumps({'Description': (token['Description']), 'Roles': [{'Name': el} for el in token['Roles']]})
        if not self.__readonly__:
            resp = self.__http__.put(CB.json(), '/v1/acl/token', data=data, params=self.__auth_param__)
            with open('generated-tokens.log', 'a+') as f:
                f.write(f"{resp['Description']}  {resp['SecretID']} \n")

    def update_token(self, consul_token, token):
        token_id = consul_token['AccessorID']
        logging.debug(consul_token)
        logging.debug(token)
        logging.info(f"[Token] Updating token '{token}', AccessorID: {token_id}\n   "
                     f"roles before: {get_token_roles_name(consul_token)}\n    "
                     f"roles after: {token['Roles']}")
        TOKEN_UPDATED.append(token['Description'])
        data = json.dumps({'Description': (token['Description']), 'Roles': [{'Name': el} for el in token['Roles']]})
        if not self.__readonly__:
            self.__http__.put(CB.json(), f'/v1/acl/token/{token_id}', data=data, params=self.__auth_param__)

    def delete_token(self, token):
        token_id = token['AccessorID']
        logging.info(f"[Token] Deleting token {token['Description']}, def: {token}")
        TOKEN_DELETED.append(token['Description'])
        if not self.__readonly__:
            self.__http__.delete(CB.json(), f'/v1/acl/token/{token_id}', params=self.__auth_param__)


def process_policies(consul_acl: ConsulAclClient, data_dir: str):
    policies_map = consul_acl.list_policies()
    logging.debug(f"[Policy] Consul policies: {policies_map.keys()}")

    # Save Policy
    for file_path in glob.iglob(f'{data_dir}/**/*.hcl', recursive=True):
        file_dir, filename = os.path.split(file_path.replace('\\', '/'))
        logging.debug(f'[Policy] Processing `{file_dir} / {filename}` ...')
        policy_name = filename.replace('.hcl', '')
        policy_rules = read_file(file_path)

        policy_id = policies_map.get(policy_name)
        policy = consul_acl.get_policy(policy_id) if policy_id else None

        if policy is None:
            consul_acl.create_policy(policy_name, policy_rules)
        elif policy['Rules'] != policy_rules:
            consul_acl.update_policy_rules(policy, policy_rules)
        else:
            POLICY_OK.append(policy_name)

    # Delete Policy
    for policy_name in read_yaml_file(f'{data_dir}/policy/deleted-policies.yml')['Names']:
        policy_id = policies_map.get(policy_name)
        if policy_id:
            consul_acl.delete_policy(policy_id, policy_name)
        else:
            POLICY_OK.append(policy_name)


def get_role_policies_names(consul_role: dict) -> list:
    return [el['Name'] for el in consul_role['Policies']] if 'Policies' in consul_role else []


def process_roles(consul_acl: ConsulAclClient, data_dir):
    consul_roles = consul_acl.list_roles()
    logging.debug(f"[Role] Consul roles: {consul_roles.keys()}")

    all_roles = []
    for file_path in [f'{data_dir}/role/app-roles.yml', f'{data_dir}/role/user-roles.yml']:
        all_roles += read_yaml_file(file_path)
    check_uniqueness(all_roles, 'Name', 'There are at least two Roles with name:')

    # Save Role
    for role in all_roles:
        consul_role = consul_roles.get(role['Name'])
        role['Policies'].sort()

        if consul_role is None:
            consul_acl.create_role(role)
        elif consul_role['Description'] != role['Description'] \
                or not are_list_equal(role['Policies'], get_role_policies_names(consul_role)):
            consul_acl.update_role(consul_role, role)
        else:
            ROLE_OK.append(role['Name'])

    # Delete role
    for role_name in read_yaml_file(f'{data_dir}/role/deleted-roles.yml')['Names']:
        role = consul_roles.get(role_name)
        if role:
            consul_acl.delete_role(role)
        else:
            ROLE_OK.append(role_name)


def get_token_roles_name(consul_token: dict) -> list:
    return [item['Name'] for item in consul_token['Roles']] if 'Roles' in consul_token else []


def process_tokens(consul_acl: ConsulAclClient, data_dir: str):
    consul_tokens = consul_acl.list_tokens()
    logging.debug(f"[Token] Consul tokens: {consul_tokens.keys()}")

    all_tokens = []
    for file_path in [f'{data_dir}/token/app-tokens.yml', f'{data_dir}/token/user-tokens.yml']:
        all_tokens += read_yaml_file(file_path)
    check_uniqueness(all_tokens, 'Description', 'There are at least two Tokens with description:')

    # Save Token
    for token in all_tokens:
        token_dsc = token['Description']
        consul_token = consul_tokens.get(token_dsc)

        if consul_token is None:
            consul_acl.create_token(token)
        elif not are_list_equal(token['Roles'], get_token_roles_name(consul_token)):
            consul_acl.update_token(consul_token, token)
        else:
            TOKEN_OK.append(token_dsc)

    # Delete token
    for token_name in read_yaml_file(f'{data_dir}/token/deleted-tokens.yml')['Descriptions']:
        token = consul_tokens.get(token_name)
        if token:
            consul_acl.delete_token(token)
        else:
            TOKEN_OK.append(token_name)


def check_uniqueness(objects, property_name, error_msg_prefix):
    copy = []
    for token in objects:
        dsc = token[property_name]
        if dsc in copy:
            assert False, f"'{error_msg_prefix} {dsc}'"
        copy.append(dsc)


def process_consul_acl():
    consul_acl = ConsulAclClient(cmd_args)
    data_dir = cmd_args.data_dir

    process_policies(consul_acl, data_dir)
    process_roles(consul_acl, data_dir)
    process_tokens(consul_acl, data_dir)

    logging.info('--- POLICIES CHANGES ---')
    logging.info(f'[Police] OK num: {len(POLICY_OK)}, list: {POLICY_OK}')
    logging.info(f'[Police] created num: {len(POLICY_CREATED)}, list: {POLICY_CREATED}')
    logging.info(f'[Police] updated num: {len(POLICY_UPDATED)}, list: {POLICY_UPDATED}')
    logging.info(f'[Police] deleted num: {len(POLICY_DELETED)}, list: {POLICY_DELETED}')

    logging.info('--- ROLES CHANGES ---')
    logging.info(f'[Role] OK num: {len(ROLE_OK)}, list: {ROLE_OK}')
    logging.info(f'[Role] created num: {len(ROLE_CREATED)}, list: {ROLE_CREATED}')
    logging.info(f'[Role] updated num: {len(ROLE_UPDATED)}, list: {ROLE_UPDATED}')
    logging.info(f'[Role] deleted num: {len(ROLE_DELETED)}, list: {ROLE_DELETED}')

    logging.info('--- TOKENS CHANGES ---')
    logging.info(f'[Token] OK num: {len(TOKEN_OK)}, list: {TOKEN_OK}')
    logging.info(f'[Token] created num: {len(TOKEN_CREATED)}, list: {TOKEN_CREATED}')
    logging.info(f'[Token] updated num: {len(TOKEN_UPDATED)}, list: {TOKEN_UPDATED}')
    logging.info(f'[Token] deleted num: {len(TOKEN_DELETED)}, list: {TOKEN_DELETED}')

    logging.info('--- ACL SUMMARY ---')
    logging.info(f'[SUM] OK num: {len(POLICY_OK) + len(ROLE_OK) + len(TOKEN_OK)}')
    logging.info(f'[SUM] created num: {len(POLICY_CREATED) + len(ROLE_CREATED) + len(TOKEN_CREATED)}')
    logging.info(f'[SUM] updated num: {len(POLICY_UPDATED) + len(ROLE_UPDATED) + len(TOKEN_UPDATED)}')
    logging.info(f'[SUM] deleted num: {len(POLICY_DELETED) + len(ROLE_DELETED) + len(TOKEN_DELETED)}')
    logging.info('---------------')


def parse_arguments():
    parser = argparse.ArgumentParser(description='Consul ACL Configurer')
    parser.add_argument('--host', action='store', required=True, help='Consul host')
    parser.add_argument('--port', action='store', required=True, help='Consul port')
    parser.add_argument('--scheme', action='store', required=False, help='Consul scheme', default='http')
    parser.add_argument('--token', action='store', required=True, help='Consul Token')
    parser.add_argument('--data-dir', action='store', required=True, help='Path to Consul Key Value Data')
    parser.add_argument('--readonly', action='store_true', help='No updates will be made')

    args = parser.parse_args()

    logging.info(f'Arguments: {args}')
    return args


if __name__ == "__main__":
    configure_logger()
    cmd_args = parse_arguments()
    process_consul_acl()
