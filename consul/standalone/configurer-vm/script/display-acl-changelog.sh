#!/usr/bin/env bash

consul_configurer_secret_id=$(awk '/SecretID/{print $2}' /vagrant/consul-vm/acl-configurer.log)

echo "Running Consul ACL configurer ... "
python3 /vagrant/configurer-vm/script/consul-acl-configurer.py --host 172.20.20.10 --port 80 --token ${consul_configurer_secret_id} --data-dir /vagrant/configurer-vm/data/acl --readonly
