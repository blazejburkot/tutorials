#!/usr/bin/python3

# -*- coding: utf-8 -*-

# pip install python-consul
# pip install jproperties

import argparse
import glob
import logging
import logging.config
import os
from datetime import datetime

import consul
from jproperties import Properties

APP_PROPS_FILENAME = 'application.properties'
DELETED_PROPS_FILENAME = 'deleted.properties'

KV_OK = []
KV_CREATED = []
KV_UPDATED = []
KV_DELETED = []


def configure_logger():
    log_format = '[%(levelname)-5s][%(asctime)s] - %(message)s'
    log_date_format = '%Y-%m-%d %H:%M:%S'
    log_filename = f'logs/consul-kv-configurer_{datetime.now().strftime("%Y-%m-%d_%H-%M")}.log'

    formatter = logging.Formatter(log_format, datefmt=log_date_format)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    os.makedirs(os.path.dirname(log_filename), exist_ok=True)
    logging.basicConfig(filename=log_filename, level=logging.DEBUG, format=log_format, datefmt=log_date_format)
    logging.root.addHandler(console_handler)


def read_properties(file_path):
    props = Properties()
    with open(file_path, "rb") as file:
        props.load(file, "utf-8")
    dict0 = {}
    for key, value in props.items():
        dict0[key] = value.data
    return dict0


def len_safe(arg):
    return len(arg) if arg is not None else 0


class ConsulKvClient:
    def __init__(self, args):
        self.__server__ = consul.Consul(host=args.host, port=args.port, scheme=args.scheme, verify=True, cert=None)
        self.__token__ = args.token
        self.__http__ = self.__server__.http
        self.__auth_param__ = [('token', args.token)]
        self.__readonly__ = args.readonly

    def get_key(self, key):
        index, data = self.__server__.kv.get(key, token=self.__token__)
        return data['Value'].decode('utf-8') if data is not None else None

    def update_key(self, key, value, old_value):
        if len_safe(value) > 20 or len_safe(old_value) > 20:
            logging.info(f"[KV] Updating KV key: {key}\n   new value: {repr(value)} \n   old value: {repr(old_value)}")
        else:
            logging.info(f"[KV] Updating KV key: {key}, new value: {repr(value)}, old value {repr(old_value)}")
        if not self.__readonly__:
            self.__server__.kv.put(key, value, token=self.__token__)

    def delete_key(self, key, old_value):
        logging.info(f"[KV] Deleting key: {key}, old value '{old_value}' ")
        KV_DELETED.append(key)
        if not self.__readonly__:
            self.__server__.kv.delete(key, token=self.__token__)


def remove_from_file_path(path, root_dir: str, filename=None):
    root = f'{root_dir}/' if not root_dir.endswith('/') else root_dir
    app_name: str = path.replace('\\', '/').replace(root, '')
    if filename is not None:
        app_name = app_name.replace(filename, '')
    return app_name[:-1] if app_name.endswith('/') else app_name


def update_application_properties(consul_ks, data_dir):
    app_props_filename = 'application.properties'
    for file_path in glob.iglob(f'{data_dir}/**/{app_props_filename}', recursive=True):
        logging.debug(f'Processing `{file_path}` ...')
        app_name = remove_from_file_path(file_path, data_dir, app_props_filename)
        app_props = read_properties(file_path)

        update_app_properties(consul_ks, app_name, app_props)


def process_files(consul_ks, data_dir):
    for file_path in glob.iglob(data_dir + '/**/*', recursive=True):
        if file_path.endswith(APP_PROPS_FILENAME) or file_path.endswith(DELETED_PROPS_FILENAME) \
                or os.path.isdir(file_path):
            continue
        logging.debug(f'Processing file: {file_path} ...')
        property_name = remove_from_file_path(file_path, data_dir)
        with open(file_path, 'r') as file:
            file_content = file.read()

        update_property_if_value_is_different(consul_ks, property_name, file_content)


def process_properties(consul_ks: ConsulKvClient, data_dir, filename, process_property_file_fn):
    for file_path in glob.iglob(f'{data_dir}/**/{filename}', recursive=True):
        logging.debug(f'Processing deleted properties: `{file_path}` ...')
        app_name = remove_from_file_path(file_path, data_dir, filename)
        app_props = read_properties(file_path)

        process_property_file_fn(consul_ks, app_name, app_props)


def update_app_properties(consul_ks: ConsulKvClient, app_name, app_props):
    logging.debug(f'App `{app_name}` properties: {app_props}')
    for key, value in app_props.items():
        prop_fullname = f'{app_name}/{key}'
        update_property_if_value_is_different(consul_ks, prop_fullname, value)


def update_property_if_value_is_different(consul_kv: ConsulKvClient, prop_fullname, value):
    consul_value = consul_kv.get_key(prop_fullname)
    if consul_value is None or consul_value != value:
        consul_kv.update_key(prop_fullname, value, consul_value)
        (KV_CREATED if consul_value is None else KV_UPDATED).append(prop_fullname)
    else:
        KV_OK.append(prop_fullname)


def remove_app_properties(consul_kv: ConsulKvClient, app_name, app_props):
    for key, value in app_props.items():
        prop_fullname = '%s/%s' % (app_name, key)
        consul_value = consul_kv.get_key(prop_fullname)
        if consul_value is not None:
            consul_kv.delete_key(prop_fullname, consul_value)
        else:
            KV_OK.append(prop_fullname)


def process_consul_kv():
    consul_ks = ConsulKvClient(cmd_args)

    process_properties(consul_ks, cmd_args.data_dir, APP_PROPS_FILENAME, update_app_properties)
    process_properties(consul_ks, cmd_args.data_dir, DELETED_PROPS_FILENAME, remove_app_properties)
    process_files(consul_ks, cmd_args.data_dir)

    logging.info('--- KeyValue CHANGES ---')
    logging.info(f"[KV OK num: {len(KV_OK)}] Properties which were not changed: {KV_OK}")
    logging.info(f"[KV CREATED num: {len(KV_CREATED)}] Properties which were updated: {KV_CREATED}")
    logging.info(f"[KV CHANGED num: {len(KV_UPDATED)}] Properties which were updated: {KV_UPDATED}")
    logging.info(f"[KV DELETED num: {len(KV_DELETED)}] Properties which were deleted: {KV_DELETED}")


def parse_arguments():
    parser = argparse.ArgumentParser(description='Consul KV Configurer')

    parser.add_argument('--host', action='store', required=True, help='Consul host')
    parser.add_argument('--port', action='store', required=True, help='Consul port')
    parser.add_argument('--scheme', action='store', required=False, help='Consul scheme', default='http')
    parser.add_argument('--token', action='store', required=True, help='Consul Token')
    parser.add_argument('--data-dir', action='store', required=True, help='Path to Consul Key Value Data')
    parser.add_argument('--readonly', action='store_true', help='No updates will be made')

    args = parser.parse_args()
    logging.info('Arguments: %s', args)

    return args


if __name__ == "__main__":
    configure_logger()

    cmd_args = parse_arguments()

    process_consul_kv()
