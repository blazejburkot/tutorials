package com.example.consul;

import lombok.Data;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Data
@CommonsLog
@RefreshScope
@Configuration
@ConfigurationProperties("app")
public class RefreshableApplicationProperties {
    private String property; // Consul key: '/app/app-example/app.property'
}
