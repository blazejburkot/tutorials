package com.example.consul;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.OperationException;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.kv.model.GetValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.consul.config.ConsulConfigProperties;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class PropertiesController {

    @Autowired
    private ConsulClient consulClient;

    @Autowired
    private ConsulConfigProperties properties;

    @Autowired
    private RefreshableApplicationProperties refreshableApplicationProperties;

    @Value("${app.property}")
    private String applicationProperty;


    @GetMapping(value = "/property", produces = MediaType.TEXT_HTML_VALUE)
    String getApplicationProperty() {
        return String.format("<p>Read 'app.property'</p>" +
                "<p>using RefreshableApplicationProperties class: <b>%s</b></p>" +
                "<p>using @Value(): <b>%s</b> </p>" +
                "<p>using Consul API: <b>%s</b> </p> ",
            refreshableApplicationProperties.getProperty(),
            applicationProperty,
            readProperty("app/app-example/app.property"));
    }

    private String readProperty(final String key) {
        try {
            final Response<GetValue> response = consulClient.getKVValue(key, properties.getAclToken());
            final GetValue getValue = response.getValue();
            return getValue.getDecodedValue();
        } catch (OperationException ex) {
            if (ex.getStatusCode() != 403) {
                throw ex;
            }
            return ex.toString();
        }
    }
}
