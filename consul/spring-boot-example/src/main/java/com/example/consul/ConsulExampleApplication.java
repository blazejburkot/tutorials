package com.example.consul;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
@EnableDiscoveryClient
public class ConsulExampleApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ConsulExampleApplication.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }

    @GetMapping("/")
    public List<String> urls(final HttpServletRequest request) {
        final String homeUri = request.getRequestURL().toString();

        return Arrays.asList(
                homeUri + "instances",
                homeUri + "services",
                homeUri + "property",
                homeUri + "actuator"
        );
    }
}
