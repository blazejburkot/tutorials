package com.example.consul;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class DiscoveryClientController {
    private final String selfApplicationId;
    private final DiscoveryClient discoveryClient;

    public DiscoveryClientController(final DiscoveryClient discoveryClient,
                                     @Value("${spring.application.name}") final String selfApplicationId) {
        this.selfApplicationId = selfApplicationId;
        this.discoveryClient = discoveryClient;
    }

    @GetMapping("/instances")
    public List<String> instances() {
        return discoveryClient.getInstances(selfApplicationId).stream()
                .map(this::mapServiceInstanceToString)
                .collect(Collectors.toList());
    }

    @GetMapping("/services")
    public List<String> services() {
        return discoveryClient.getServices();
    }

    private String mapServiceInstanceToString(final ServiceInstance inst) {
        return String.format("ser: %s,  host: %s, inst: %s", inst.getServiceId(), inst.getHost(), inst.getInstanceId());
    }
}
