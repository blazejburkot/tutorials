# Consul

## Docs
- [HashiCorp Tutorial](https://learn.hashicorp.com/consul)
- [Interactive Demo](https://demo.consul.io/ui/dc1/services)
- [Consul Documentation](https://www.consul.io/docs/index.html)

## Standalone Consul

### Local Environment Requirements
1. Installed [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
1. Installed [Vagrant](http://downloads.vagrantup.com/)
1. Installed [vagrant-hosts](https://github.com/oscar-stack/vagrant-hosts#installation)

In case of Windows 10:   
    1. Disable Hyper-V. Run `dism.exe /Online /Disable-Feature:Microsoft-Hyper-V`

### Run locally
1. Exec `vagrant up consul` in project's root directory
1. Open `http://172.20.20.10/` in browser, you should see Consul's 'Services' page
1. Exec `vagrant up configurer` in project's root directory
1. Update `token` property in `spring-boot-example/src/main/resources/bootstrap.yml` using value of '[App] app-example' Token from http://172.20.20.10/ui/dc1/acls/tokens
1. Run `ConsulExampleApplication.class`

In case of problems, checkout:
```
logs:
    - /vagrant/consul-vm/consul.log
    - /var/log/nginx/*

processes:
    - systemctl status nginx
    - consul info
```

#### Recreate Audit pages 
```
vagrant ssh consul
sudo /vagrant/consul-vm/script/create-audit-pages.sh
```

#### Register custom healthcheck
```
vagrant ssh consul
sudo /vagrant/consul-vm/script/register-custom-healthcheck.sh
```

### Clean up
1. Run `vagrant destroy -f`
