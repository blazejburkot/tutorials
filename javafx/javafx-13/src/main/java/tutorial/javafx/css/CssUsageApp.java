package tutorial.javafx.css;

import javafx.application.Application;
import tutorial.javafx.util.SimpleFxmlApplication;

public class CssUsageApp {

    public static void main(final String[] args) {
        Application.launch(SimpleFxmlApplication.class, "/css/css-example.fxml");

        // Stylesheet also can be used:
        // scene.getStylesheets().add("styles.css");
        // vBox.getStylesheets().add("styles.css");
    }
}
