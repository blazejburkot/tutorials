package tutorial.javafx.util;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class SimpleFxmlApplication extends Application {

    @Override
    public void start(final Stage stage) throws Exception {
        final List<String> parameters = getParameters().getRaw();
        if (parameters.size() != 1) {
            throw new IllegalArgumentException("Wrong parameters: " + parameters);
        }

        stage.setScene(createScene(parameters.get(0)));

        stage.show();
    }

    private Scene createScene(final String resourceName) throws IOException {
        final FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource(resourceName));
        final VBox vBox = fxmlLoader.load();

        return new Scene(vBox);
    }
}
