package tutorial.javafx.helloworld;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HelloWorldJavaApp extends Application {

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(final Stage stage) {
        stage.initStyle(StageStyle.DECORATED);
        stage.setTitle("Hello World");

        final Scene scene = createScene();
        stage.setScene(scene);

        stage.show();
    }

    private Scene createScene() {
        final String javaVersion = System.getProperty("java.version");
        final String javafxVersion = System.getProperty("javafx.version");
        final String labelTxt = String.format("Hello World, Java! (java version: %s, JavaFX version: %s)", javaVersion, javafxVersion);

        final HBox hBox = new HBox(new Label(labelTxt));
        hBox.setAlignment(Pos.CENTER);

        final VBox vBox = new VBox(hBox);
        vBox.setAlignment(Pos.CENTER);

        return new Scene(vBox, 400, 200);
    }
}
