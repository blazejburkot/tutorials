package tutorial.javafx.helloworld;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class HelloWorldFxmlApp extends Application {

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(final Stage stage) throws IOException {
        stage.setTitle("Hello World");

        final Scene scene = createScene();
        stage.setScene(scene);

        stage.show();
    }

    private Scene createScene() throws IOException {
        final FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/helloworld/hello-world-fxml.fxml"));
        final VBox vBox = fxmlLoader.load();

        return new Scene(vBox, 400, 200);
    }
}
