package tutorial.javafx.helloworld;

public class HelloWorldFxmlController {

    public String getLabelText() {
        final String javaVersion = System.getProperty("java.version");
        final String javafxVersion = System.getProperty("javafx.version");
        return String.format("Hello World, Fxml! (java version: %s, JavaFX version: %s)", javaVersion, javafxVersion);
    }
}
