package tutorial.javafx.component.inputfield;

import javafx.application.Application;
import tutorial.javafx.util.SimpleFxmlApplication;

public class InputFieldApp {
    public static void main(String[] args) {
        Application.launch(SimpleFxmlApplication.class, "/component/inputfield/input-field-example.fxml");
    }
}
