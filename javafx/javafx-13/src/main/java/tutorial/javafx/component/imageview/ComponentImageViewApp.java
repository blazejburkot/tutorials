package tutorial.javafx.component.imageview;

import javafx.application.Application;
import tutorial.javafx.util.SimpleFxmlApplication;

public class ComponentImageViewApp {
    public static void main(String[] args) {
        Application.launch(SimpleFxmlApplication.class, "/component/imageview/image-example.fxml");
    }
}
