package tutorial.javafx.component.menu;

import javafx.application.Application;
import tutorial.javafx.util.SimpleFxmlApplication;

public class MenuApp {
    public static void main(String[] args) {
        Application.launch(SimpleFxmlApplication.class, "/component/menu/menu-example.fxml");
    }
}
