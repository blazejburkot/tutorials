package tutorial.javafx.component.button;

import javafx.application.Application;
import tutorial.javafx.util.SimpleFxmlApplication;

public class ButtonApp {
    public static void main(String[] args) {
        Application.launch(SimpleFxmlApplication.class, "/component/button/button-example.fxml");
    }
}
