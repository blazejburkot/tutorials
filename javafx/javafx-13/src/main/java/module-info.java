module example_javafx {
    requires javafx.controls;
    requires javafx.fxml;

    opens tutorial.javafx.helloworld to javafx.graphics, javafx.fxml;
    opens tutorial.javafx.util;
    opens tutorial.javafx.i18n;
}
