package example.simple;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

@Slf4j
public class SimpleKafkaConsumer {

    public static void main(String[] args) {
        final Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", "localhost:9092");
        kafkaProps.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        kafkaProps.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
//        kafkaProps.put("group.id", "App abc"); // Fixed ID Value
        kafkaProps.put("group.id", "App abc " + System.currentTimeMillis()); // Different Id every time

        kafkaProps.put("auto.offset.reset", "earliest");


        try (final KafkaConsumer<String, String> consumer = new KafkaConsumer<>(kafkaProps)) {
            consumer.subscribe(Collections.singletonList(SimpleKafkaProducer.KAFKA_TOPIC));
            while (true) {
                log.info("Polling ... ");
                final ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(10));
                for (ConsumerRecord<String, String> record : records) {
                    log.info("topic = {}, partition = {}, offset = {}, key = {}, value = {}",
                            record.topic(), record.partition(), record.offset(), record.key(), record.value());

                    final TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
                    final OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(record.offset() + 1);
//                    consumer.commitSync(Map.of(topicPartition, offsetAndMetadata));
                }
            }
        }
    }
}
