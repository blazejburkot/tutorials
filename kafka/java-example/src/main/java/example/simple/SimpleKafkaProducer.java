package example.simple;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.concurrent.Future;

@Slf4j
public class SimpleKafkaProducer {
    public static final String KAFKA_TOPIC = "some.topic";

    public static void main(String[] args) {
        final Properties kafkaProps = new Properties();
        kafkaProps.put("bootstrap.servers", "localhost:9092");
        kafkaProps.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        kafkaProps.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        final KafkaProducer<String, String> producer = new KafkaProducer<>(kafkaProps);

        final ProducerRecord<String, String> record = new ProducerRecord<>(KAFKA_TOPIC, "some.key", "some.value");

        try {
            final Future<RecordMetadata> metadataFuture = producer.send(record);
            final RecordMetadata recordMetadata = metadataFuture.get();
            log.info("metadata: " + recordMetadata);
        } catch (Exception e) {
            e.printStackTrace();
        }

        producer.send(record, (recordMetadata, e) -> log.info("Future metadata: {}, ex: {}", recordMetadata, e));

        producer.close();
    }
}
