# Apache Kafka

https://kafka.apache.org/

## Install
- [Standalone server on Debian](standalone-srver/Vagrantfile)

## Read
- http://kafka.apache.org/
- [notes](https://docs.google.com/document/d/1dGUitOlJTQlHsAC6_GuCpxsA4FG0ul2aftvP7KegLhs/edit)
