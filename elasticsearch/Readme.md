# Elasticsearch

## Install
- [Manually on Debian](vagrant/es-debian-shell/install-elaticsearch.sh)

## Client usage
- [REST API examples](client-rest-api-examples.md)

## Administration
- [REST API examples](administraction-rest-api.md)

## Reading
- [The Definitive Guide to Elasticsearch](https://github.com/elastic/elasticsearch-definitive-guide) ([notes](https://docs.google.com/document/d/1CC7mtaxnPoq8736FgWQjSX9TEdNpopcxxNpqxTqPrro/edit#))