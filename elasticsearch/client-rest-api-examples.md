# Client usage

1. Add document
`megacorp` is the index name
`employee` is the type name
```
PUT /megacorp/employee/1
{"first_name": "John", "last_name": "Smith", "interests": ["sports","music"]}
```

1. Get document by id `GET /megacorp/employee/1`
1. Get part of the document `GET /megacorp/employee/1?_source=first_name`
1. Check if document with id is present `HEAD /megacorp/employee/1`
1. Get document by *last_name* field `GET /megacorp/employee/_search?q=last_name:Smith`
1. Get document by *last_name* field
```
GET /megacorp/employee/_search
{
    "query" : {
        "match" : {
            "last_name" : "Smith"
}}}
```
1. Aggregate field's terms
```
GET /megacorp/employee/_search
{
    "aggs": {
        "all_interests": {
            "terms": { "field": "interests" }
}}}
```