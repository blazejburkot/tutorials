# Elasticsearch on Debian

## Directory layout of Debian package
The Debian package places config files, logs, and the data directory in the appropriate locations for a Debian-based system:

| Type  | Description | Default Location | Setting |
| --- | --- | --- | --- |
| home | Elasticsearch home directory or $ES_HOME | /usr/share/elasticsearch | |
| bin | Binary scripts including elasticsearch to start a node and elasticsearch-plugin to install plugins | /usr/share/elasticsearch/bin | |
| conf | Configuration files including elasticsearch.yml | /etc/elasticsearch | ES_PATH_CONF | |
| conf | Environment variables including heap size, file descriptors. | /etc/default/elasticsearch | |
| data | The location of the data files of each index / shard allocated on the node. Can hold multiple locations. | /var/lib/elasticsearch | path.data |
| logs | Log files location. | /var/log/elasticsearch | path.logs |
| plugins | Plugin files location. Each plugin will be contained in a subdirectory. | /usr/share/elasticsearch/plugins | |
| repo | Shared file system repository locations. Can hold multiple locations. A file system repository can be placed in to any subdirectory of any directory specified here. | Not configured | path.repo |
