#!/bin/bash

set -e

#######################################################
# INSTALL OpenJDK 1.8
echo "deb http://http.debian.net/debian jessie-backports main" | sudo tee -a /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get install -y -t jessie-backports openjdk-8-jdk
sudo apt-get install -y net-tools wget curl

java -version

#######################################################
# INSTALL ElasticSearch MANUALLY

ES_VERSION="elasticsearch-oss-6.6.2"

wget -q https://artifacts.elastic.co/downloads/elasticsearch/${ES_VERSION}.deb
wget -q https://artifacts.elastic.co/downloads/elasticsearch/${ES_VERSION}.deb.sha512
shasum -a 512 -c ${ES_VERSION}.deb.sha512
sudo dpkg -i ${ES_VERSION}.deb

echo "network.host: 0.0.0.0" | sudo tee -a /etc/elasticsearch/elasticsearch.yml

#######################################################
# RUNNING ElasticSearch
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service
