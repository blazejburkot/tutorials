package hibernate4.l2cache.ehcache;

import lombok.Data;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class CacheableEntity {

    @Id
    private int id;

    private String value;

    public CacheableEntity(final String value) {
        this.value = value;
    }
}
