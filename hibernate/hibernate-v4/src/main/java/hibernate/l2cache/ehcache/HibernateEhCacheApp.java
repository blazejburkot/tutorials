package hibernate4.l2cache.ehcache;

import hibernate4.common.HibernateApplicationBase;

public class HibernateEhCacheApp extends HibernateApplicationBase {

    public HibernateEhCacheApp() {
        super("hibernate-with-ehcache.cfg.xml", CacheableEntity.class);
    }

    public static void main(String[] args) {
        final HibernateEhCacheApp hibernate = new HibernateEhCacheApp();
        final CacheableEntity entity = new CacheableEntity("some value");

        hibernate.persist(entity);

        hibernate.getById(entity.getClass(), entity.getId());

        hibernate.getById(entity.getClass(), entity.getId());

        hibernate.close();
    }
}
