package hibernate4.othersannotation;

import hibernate4.common.HibernateApplicationBase;
import org.assertj.core.api.Assertions;
import org.hibernate.Session;

import java.util.Collections;

public class HibernateOthersAnnotationUsage extends HibernateApplicationBase {

    public HibernateOthersAnnotationUsage() {
        super(EntityData.class);
    }

    public static void main(String[] args) {
        final HibernateOthersAnnotationUsage hibernate = new HibernateOthersAnnotationUsage();

        naturalId(hibernate);

        hibernate.close();
    }

    private static void naturalId(final HibernateOthersAnnotationUsage hibernate) {
        final EntityData entityData = new EntityData();
        entityData.setNaturalId(1234);
        entityData.setStrings(Collections.singletonList("123"));

        hibernate.persist(entityData);

        System.out.println("\n> get byNaturalId");
        Session session = hibernate.getSessionFactory().openSession();
        final EntityData entityData2 =
                (EntityData) session.byNaturalId(EntityData.class)
                        .using("naturalId", 1234)
                        .load();
        entityData2.getStrings().forEach(el -> {});
        session.close();

        System.out.println("\n> get bySimpleNaturalId");
        session = hibernate.getSessionFactory().openSession();
        final EntityData entityData3 =
                (EntityData) session.bySimpleNaturalId(EntityData.class)
                        .load(1234);
        entityData3.getStrings().forEach(el -> {});
        session.close();

        Assertions.assertThat(entityData)
                .isEqualToComparingFieldByFieldRecursively(entityData2)
                .isEqualToComparingFieldByFieldRecursively(entityData3);
    }
}
