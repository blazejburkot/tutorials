package hibernate4.othersannotation;

import lombok.Data;
import org.hibernate.annotations.NaturalId;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class EntityData {

    @Id
    private int id;

    @NaturalId
    private Integer naturalId;

    @Temporal(TemporalType.DATE)
    private Date dateType;

    @Temporal(TemporalType.TIME)
    private Date timeType;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampType;

    @ElementCollection
    @JoinTable(name = "EntityStrings")
    private List<String> strings;

    @Lob
    private String bigString;

    private String plainString;

    @Lob
    private byte[] bytes;
}
