package hibernate4.criteria;

import hibernate4.common.HibernateApplicationBase;
import hibernate4.inheritance.joinedtable.Book;
import hibernate4.inheritance.joinedtable.Book1;
import hibernate4.inheritance.joinedtable.Book2;

public class HibernateCriteriaApiBaseApp extends HibernateApplicationBase {

    public HibernateCriteriaApiBaseApp() {
        super(Book.class, Book1.class, Book2.class);
    }

    public static void main(String[] args) {

    }
}
