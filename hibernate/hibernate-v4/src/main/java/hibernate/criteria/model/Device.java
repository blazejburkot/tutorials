package hibernate4.criteria.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Device {
    @Id
    private int id;

    private String name;
}
