package hibernate4.criteria.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Address {
    @Id
    private int id;

    private String country;
    private String city;
    private String street;
    private String buildingNumber;
}
