package hibernate4.criteria.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Set;

@Data
@Entity
public class Person {

    @Id
    private int id;

    @ManyToOne
    private Gender gender;

    @OneToMany
    private Set<Address> deliveryAddress;

    @ManyToMany
    private Set<Device> devices;
}
