package hibernate4.relationships.manytomany;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@Entity
public class Book {

    @Id
    public int id;

    @ManyToMany(mappedBy = "books")
    private Set<Author> authors;

    @Column
    private String name;
}
