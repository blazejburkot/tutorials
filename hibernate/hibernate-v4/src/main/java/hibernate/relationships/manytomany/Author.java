package hibernate4.relationships.manytomany;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@Entity
public class Author {

    @Id
    private int id;

    @ManyToMany
//    @JoinTable( // this is not required
//            name = "Author_Book_Custom",
//            joinColumns = {@JoinColumn(name = "author_id")},
//            inverseJoinColumns = {@JoinColumn(name = "book_id")}
//    )
    private Set<Book> books;

    private String firstName;
    private String lastName;
}
