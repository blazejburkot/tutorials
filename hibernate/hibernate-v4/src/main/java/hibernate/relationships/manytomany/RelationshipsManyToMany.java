package hibernate4.relationships.manytomany;

import hibernate4.common.HibernateApplicationBase;

public class RelationshipsManyToMany extends HibernateApplicationBase {

    public RelationshipsManyToMany() {
        super(Book.class, Author.class);
    }

    public static void main(String[] args) {
        final RelationshipsManyToMany hibernate = new RelationshipsManyToMany();

        hibernate.close();
    }
}
