package hibernate4.relationships.onetoone.conventional;

import hibernate4.common.HibernateApplicationBase;

public class RelationshipsOneToOneConventional extends HibernateApplicationBase {

    public RelationshipsOneToOneConventional() {
        super(Book.class, ISBN.class);
    }

    public static void main(String[] args) {
        final RelationshipsOneToOneConventional hibernate = new RelationshipsOneToOneConventional();

        hibernate.close();
    }
}
