package hibernate4.relationships.onetoone.conventional;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Data
@Entity
public class ISBN implements Serializable {

    @Id
    public int id;

    @OneToOne(mappedBy = "isbn")
    private Book book;

    @Column(name="group_number") // because "group" is an invalid column name for SQL
    int group;
    int publisher;
    int title;
    int checkdigit;
}
