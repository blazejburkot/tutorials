package hibernate4.relationships.onetoone.embedded;

import lombok.Data;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Book1 {

    @Id
    public int id;

    @Embedded
    @AttributeOverrides({
//            @AttributeOverride(name = "group_number", column = @Column(name = "group_number_v2", columnDefinition = "bigint")) // this doesn't work
            @AttributeOverride(name = "group", column = @Column(name = "group_number_v2", columnDefinition = "bigint")) // should be field name
    })
    private ISBN isbn;

    @Column
    private String name;
}
