package hibernate4.relationships.onetoone.conventional;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
public class Book {

    @Id
    public int id;

    @OneToOne
    private ISBN isbn;

    @Column
    private String name;
}
