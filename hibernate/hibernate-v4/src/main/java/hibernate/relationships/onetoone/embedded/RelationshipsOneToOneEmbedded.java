package hibernate4.relationships.onetoone.embedded;

import hibernate4.common.HibernateApplicationBase;

public class RelationshipsOneToOneEmbedded extends HibernateApplicationBase {

    public RelationshipsOneToOneEmbedded() {
        super(Book1.class, Book2.class, ISBN.class);
    }

    public static void main(String[] args) {
        final RelationshipsOneToOneEmbedded hibernate = new RelationshipsOneToOneEmbedded();

        hibernate.close();
    }
}
