package hibernate4.relationships.onetoone.embedded;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Book2 {

    @Id
    public int id;

    @Embedded
    private ISBN isbn;

    @Column
    private String name;

    @Column
    private int numberOfPages;
}
