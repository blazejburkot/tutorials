package hibernate4.relationships.onetomany;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import java.util.List;

@Data
@Entity
public class Book {

    @Id
    public int id;

    @OneToMany(mappedBy = "book")
    @OrderBy("group ASC")
    private List<ISBN> isbns;

    @Column
    private String name;
}
