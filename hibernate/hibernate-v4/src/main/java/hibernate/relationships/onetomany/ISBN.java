package hibernate4.relationships.onetomany;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Entity
public class ISBN implements Serializable {

    @Id
    public int id;

    @ManyToOne
//    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name="group_number") // because "group" is an invalid column name for SQL
    int group;
    int publisher;
    int title;
    int checkdigit;
}
