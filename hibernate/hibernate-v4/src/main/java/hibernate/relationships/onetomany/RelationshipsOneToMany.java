package hibernate4.relationships.onetomany;

import hibernate4.common.HibernateApplicationBase;

public class RelationshipsOneToMany extends HibernateApplicationBase {

    public RelationshipsOneToMany() {
        super(Book.class, ISBN.class);
    }

    public static void main(String[] args) {
        final RelationshipsOneToMany hibernate = new RelationshipsOneToMany();

        hibernate.close();
    }
}
