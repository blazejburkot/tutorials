package hibernate4.inheritance.tableperclass;

import hibernate4.common.HibernateApplicationBase;

public class HibernateInheritanceTablePerClass extends HibernateApplicationBase {

    public HibernateInheritanceTablePerClass() {
        super(Book.class, Book1.class, Book2.class);
    }

    public static void main(String[] args) {
        final HibernateInheritanceTablePerClass hibernate = new HibernateInheritanceTablePerClass();

        hibernate.close();
    }
}
