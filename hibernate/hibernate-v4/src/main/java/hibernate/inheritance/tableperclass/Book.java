package hibernate4.inheritance.tableperclass;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Data
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Book {

    @Id
    private int id;

    private String publisher;
    private String title;
}
