package hibernate4.inheritance.joinedtable;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Book1 extends Book {

    private String extra_field_from_book1;
}
