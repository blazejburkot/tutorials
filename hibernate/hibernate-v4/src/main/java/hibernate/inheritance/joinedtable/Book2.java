package hibernate4.inheritance.joinedtable;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Book2 extends Book {

    private String extra_field_from_book2;
}
