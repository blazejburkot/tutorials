package hibernate4.inheritance.joinedtable;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Book {

    @Id
    private int id;

    private String publisher;
    private String title;
}
