package hibernate4.inheritance.joinedtable;

import hibernate4.common.HibernateApplicationBase;

public class HibernateInheritanceJoinedTable extends HibernateApplicationBase {

    public HibernateInheritanceJoinedTable() {
        super(Book.class, Book1.class, Book2.class);
    }

    public static void main(String[] args) {
        final HibernateInheritanceJoinedTable hibernate = new HibernateInheritanceJoinedTable();

        hibernate.close();
    }
}
