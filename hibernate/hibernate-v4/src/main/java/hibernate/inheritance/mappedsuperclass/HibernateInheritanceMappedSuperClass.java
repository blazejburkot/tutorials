package hibernate4.inheritance.mappedsuperclass;

import hibernate4.common.HibernateApplicationBase;

public class HibernateInheritanceMappedSuperClass extends HibernateApplicationBase {

    public HibernateInheritanceMappedSuperClass() {
        super(Book.class, Book1.class, Book2.class);
    }

    public static void main(String[] args) {
        final HibernateInheritanceMappedSuperClass hibernate = new HibernateInheritanceMappedSuperClass();

        hibernate.close();
    }
}
