package hibernate4.inheritance.mappedsuperclass;

import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class Book {

    @Id
    private int id;

    private String publisher;
    private String title;
}
