package hibernate4.inheritance.singletable;

import hibernate4.common.HibernateApplicationBase;

public class HibernateInheritanceSingleTable extends HibernateApplicationBase {

    public HibernateInheritanceSingleTable() {
        super(Book.class, Book1.class, Book2.class);
    }

    public static void main(String[] args) {
        final HibernateInheritanceSingleTable hibernate = new HibernateInheritanceSingleTable();

        hibernate.close();
    }
}
