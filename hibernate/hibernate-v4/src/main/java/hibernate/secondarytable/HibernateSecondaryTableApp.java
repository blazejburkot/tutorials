package hibernate4.secondarytable;

import hibernate4.common.HibernateApplicationBase;

public class HibernateSecondaryTableApp extends HibernateApplicationBase {

    public HibernateSecondaryTableApp() {
        super("/hibernate-secondary-table.cfg.xml");
    }

    public static void main(String[] args) {
        new HibernateSecondaryTableApp()
                .close();
    }
}
