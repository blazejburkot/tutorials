package hibernate4.secondarytable;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "customer")
@SecondaryTable(name = "customer_details")
//@SecondaryTables(value = @SecondaryTable(name = "customer_details"))
public class Customer {

    @Id
    public int id;

    public String name;

    @Column(table = "customer_details")
    public String address;
}
