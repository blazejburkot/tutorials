package hibernate4.common;

import lombok.Getter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.Serializable;
import java.util.List;
import java.util.function.Consumer;

public class HibernateApplicationBase {
    @Getter
    private final SessionFactory sessionFactory;

    public HibernateApplicationBase(final String hibernateConfigXmlPath) {
        this.sessionFactory = HibernateSessionFactoryCreator.create(hibernateConfigXmlPath);
    }
    public HibernateApplicationBase(final Class<?> ... annotatedClasses) {
        this.sessionFactory = HibernateSessionFactoryCreator.create("/hibernate-base.cfg.xml", annotatedClasses);
    }
    public HibernateApplicationBase(final String hibernateConfigXmlPath, final Class<?> ... annotatedClasses) {
        this.sessionFactory = HibernateSessionFactoryCreator.create(hibernateConfigXmlPath, annotatedClasses);
    }

    public <T> T getById(final Class<T> clazz, final Serializable id) {
        System.out.println("\n> getById");
        final Session session = sessionFactory.openSession();
        final Object obj = session.get(clazz, id);
        session.close();
        return (T) obj;
    }

    public void persist(final Object object) {
        System.out.println("\n> persist");
        final Session session = sessionFactory.openSession();
        final Transaction tx = session.beginTransaction();
        session.persist(object);
        tx.commit();
        session.close();
    }

    public <T> void updateObject(final Class<T> objectClass,
                                 final Serializable objectKey,
                                 final Consumer<T> consumer) {
        System.out.println("\n> update object");
        final Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        final T object = (T) session.get(objectClass, objectKey);

        final Transaction tx = session.beginTransaction();

        consumer.accept(object);

        tx.commit();
        session.close();
    }

    public <T> void doWithinTransaction(final Consumer<Session> consumer) {
        System.out.println("\n> do within transaction");
        final Session session = sessionFactory.openSession();
        final Transaction tx = session.beginTransaction();

        consumer.accept(session);

        tx.commit();
        session.close();
    }

    public void printAll(final String query) {
        System.out.println("\n> list");
        final Session session = sessionFactory.openSession();
        List list = session.createQuery(query).list();
        for (Object object : list) {
            System.out.println(object);
        }
        session.close();
    }

    public void close() {
        System.out.println("\n> close");
        sessionFactory.close();
    }
}
