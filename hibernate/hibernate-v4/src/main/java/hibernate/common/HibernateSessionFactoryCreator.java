package hibernate4.common;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Arrays;

public class HibernateSessionFactoryCreator {

    public static SessionFactory create(final String hibernateConfigXmlPath) {
        final Configuration configuration = new Configuration().configure(hibernateConfigXmlPath);
        return create(configuration);
    }

    public static SessionFactory create(final String hibernateConfigXmlPath, final Class<?> ... annotatedClasses) {
        final Configuration configuration = createConfigurationWithAnnotatedClasses(hibernateConfigXmlPath, annotatedClasses);
        return create(configuration);
    }


    public static SessionFactory create(final Configuration configuration) {
        final ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties())
                .build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    private static Configuration createConfigurationWithAnnotatedClasses(final String hibernateConfigXmlPath,
                                                                         final Class<?>... annotatedClasses) {
        final Configuration configuration = new Configuration().configure(hibernateConfigXmlPath);
        Arrays.stream(annotatedClasses).forEach(configuration::addAnnotatedClass);
        return configuration;
    }
}
