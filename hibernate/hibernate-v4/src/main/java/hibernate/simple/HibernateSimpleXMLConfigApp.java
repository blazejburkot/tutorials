package hibernate4.simple;

import hibernate4.common.HibernateApplicationBase;

public class HibernateSimpleXMLConfigApp extends HibernateApplicationBase {

    public HibernateSimpleXMLConfigApp() {
        super("/hibernate-simple.cfg.xml");
    }

    public static void main(String[] args) {
        final HibernateSimpleXMLConfigApp hibernate = new HibernateSimpleXMLConfigApp();

        messageCRUD(hibernate);

        hibernate.close();
    }

    private static void messageCRUD(final HibernateSimpleXMLConfigApp hibernate) {
        hibernate.persist(new Message("Hello, world"));
        hibernate.printAll("from Message");

        hibernate.updateObject(Message.class, 1L,
                persistedMsg -> persistedMsg.setText("Hello, new world"));
        hibernate.printAll("from Message");
    }
}
