package hibernate4.simple;

import hibernate4.common.HibernateApplicationBase;

public class HibernateWithC3P0PoolApp extends HibernateApplicationBase {

    public HibernateWithC3P0PoolApp() {
        super("/hibernate-with-c3p0.cfg.xml");
    }

    public static void main(String[] args) {
        final HibernateWithC3P0PoolApp hibernate = new HibernateWithC3P0PoolApp();

        hibernate.persist(new Message("Hello, world"));

        hibernate.close();
    }
}
