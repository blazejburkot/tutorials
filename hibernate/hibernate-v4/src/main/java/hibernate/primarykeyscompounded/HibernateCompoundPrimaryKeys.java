package hibernate4.primarykeyscompounded;

import hibernate4.common.HibernateApplicationBase;

public class HibernateCompoundPrimaryKeys extends HibernateApplicationBase {

    public HibernateCompoundPrimaryKeys() {
        super("/hibernate-compound-primary-keys.cfg.xml");
    }

    public static void main(String[] args) {
        final HibernateCompoundPrimaryKeys hibernate = new HibernateCompoundPrimaryKeys();

        hibernate.close();
    }
}
