package hibernate4.primarykeyscompounded.embeddable;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Embeddable
public class ISBN implements Serializable {

    @Column(name="group_number") // because "group" is an invalid column name for SQL
    int group;
    int publisher;
    int title;
    int checkdigit;
}
