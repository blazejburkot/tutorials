package hibernate4.primarykeyscompounded.embeddable;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Book1 {

    @Id         // any combination of these
    @EmbeddedId // two annotation is valid
    private ISBN isbn;

    @Column
    private String name;
}
