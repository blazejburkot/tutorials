package hibernate4.primarykeyscompounded.idclass;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Entity
@IdClass(ISBNIdClass.class)
public class Book2 {

    @Id
    @Column(name="group_number") // because "group" is an invalid column name for SQL
    private int group;

    @Id
    private int publisher;

    @Id
    private int title;

    @Id
    private int checkdigit;

    @Column
    private String name;
}
