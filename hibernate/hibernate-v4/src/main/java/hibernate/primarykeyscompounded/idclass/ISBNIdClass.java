package hibernate4.primarykeyscompounded.idclass;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ISBNIdClass implements Serializable {

    private int group;
    private int publisher;
    private int title;
    private int checkdigit;
}
