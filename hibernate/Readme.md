# Hibernate

- [L2Cache](hibernate-v4/src/main/java/hibernate/l2cache/ehcache/CacheableEntity.java)
- [Criteria TODO](TODO)
- Inheritance
    - [Mapped superclass](hibernate-v4/src/main/java/hibernate/inheritance/mappedsuperclass/Book1.java)
    - [Joined table](hibernate-v4/src/main/java/hibernate/inheritance/joinedtable/HibernateInheritanceJoinedTable.java)
    - [Single table](hibernate-v4/src/main/java/hibernate/inheritance/singletable/HibernateInheritanceSingleTable.java)
    - [Table per class](hibernate-v4/src/main/java/hibernate/inheritance/tableperclass/HibernateInheritanceTablePerClass.java)
- Primary keys compounded
    - [Embeddable](hibernate-v4/src/main/java/hibernate/primarykeyscompounded/embeddable/Book1.java)
    - [IdClass](hibernate-v4/src/main/java/hibernate/primarykeyscompounded/idclass/Book2.java)
- relationships
    - One to one
        - [Conventional](hibernate-v4/src/main/java/hibernate/relationships/onetoone/conventional/RelationshipsOneToOneConventional.java)
        - [Embedded](hibernate-v4/src/main/java/hibernate/relationships/onetoone/embedded/RelationshipsOneToOneEmbedded.java)
    - [One to many](hibernate-v4/src/main/java/hibernate/relationships/onetomany/RelationshipsOneToMany.java)
    - [Many to many](hibernate-v4/src/main/java/hibernate/relationships/manytomany/RelationshipsManyToMany.java)
- [Secondary table](hibernate-v4/src/main/java/hibernate/secondarytable/Customer.java)
- [Rare annotation](hibernate-v4/src/main/java/hibernate/othersannotation/EntityData.java)
