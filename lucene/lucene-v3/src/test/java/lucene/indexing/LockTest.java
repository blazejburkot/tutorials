package lucene.indexing;

import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class LockTest {

    @Rule
    public TemporaryFolder tempDir = new TemporaryFolder();

    private Directory indexDirectory;

    @Before
    public void setUp() throws IOException {
        final File indexDir = tempDir.newFolder("index");
        indexDirectory = FSDirectory.open(indexDir);
    }

    @Test
    public void testWriteLock() throws IOException {
        IndexWriter writer1 = createIndexWriter();
        IndexWriter writer2 = null;

        try {
            writer2 = createIndexWriter();
            fail("Exception should be thrown: only one IndexWriter allowed at once");
        } catch (LockObtainFailedException e) {
            e.printStackTrace(System.out);
        } finally {
            writer1.close();
            assertThat(writer2).isNull();
            tempDir.delete();
        }
    }

    private IndexWriter createIndexWriter() throws IOException {
        final IndexWriter indexWriter = new IndexWriter(indexDirectory, new SimpleAnalyzer(), IndexWriter.MaxFieldLength.UNLIMITED);
        indexWriter.setInfoStream(System.out);
        return indexWriter;
    }
}
