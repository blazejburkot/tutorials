package lucene.indexing;

import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Index;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class IndexingTest {
    private String[] ids = {"1", "2"};
    private String[] unindexed = {"Netherlands", "Italy"};
    private String[] unstored = {"Amsterdam has lots of bridges", "Venice has lots of canals"};
    private String[] text = {"Amsterdam", "Venice"};

    private Directory directory;

    private IndexWriter writer;
    private IndexReader reader;

    @Before
    public void setUp() throws Exception {
        directory = new RAMDirectory();

        writer = new IndexWriter(directory, new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.UNLIMITED);
        for (int i = 0; i < ids.length; i++) {
            final Document doc = new Document();
            doc.add(new Field("id", ids[i], Store.YES, Index.NOT_ANALYZED));
            doc.add(new Field("country", unindexed[i], Store.YES, Index.NO));
            doc.add(new Field("contents", unstored[i], Store.NO, Index.ANALYZED));
            doc.add(new Field("city", text[i], Store.YES, Index.ANALYZED));
            writer.addDocument(doc);
        }
        writer.commit();
//        reader = IndexReader.open(directory);
        reader = writer.getReader();
    }

    @After
    public void cleanUp() throws IOException {
        writer.close();
        reader.close();
    }

    @Test
    public void sanityChecks() throws IOException {
        assertThat(writer.numDocs()).isEqualTo(ids.length);
        assertThat(reader.numDocs()).isEqualTo(ids.length);
        assertThat(reader.numDocs()).isEqualTo(ids.length);

        assertThat(hitCount("city", "Amsterdam")).isEqualTo(1);
        assertThat(hitCount("contents", "bridges")).isEqualTo(1);
    }

    @Test
    public void testDeleteBeforeOptimize() throws IOException {
        writer.deleteDocuments(new Term("id", "1"));
        writer.commit();

        assertThat(writer.hasDeletions()).isTrue();
        assertThat(writer.maxDoc()).isEqualTo(2);
        assertThat(writer.numDocs()).isEqualTo(1);
    }

    @Test
    public void testDeleteAfterOptimize() throws IOException {
        writer.deleteDocuments(new Term("id", "1"));
        writer.optimize();
        writer.commit();

        assertThat(writer.hasDeletions()).isFalse();
        assertThat(writer.maxDoc()).isEqualTo(1);
        assertThat(writer.numDocs()).isEqualTo(1);
    }

    @Test
    public void testUpdate() throws IOException {
        final Document doc = new Document();
        doc.add(new Field("id", "1", Store.YES, Index.NOT_ANALYZED));
        doc.add(new Field("country", "Netherlands", Store.YES, Index.NO));
        doc.add(new Field("contents", "Den Haag has a lot of museums", Store.NO, Index.ANALYZED));
        doc.add(new Field("city", "Den Haag", Store.YES, Index.ANALYZED));

        writer.updateDocument(new Term("id", "1"), doc);       // Replace original document with new version
        writer.close();

        assertThat(hitCount("city", "Amsterdam")).isEqualTo(0);
        assertThat(hitCount("city", "Haag")).isEqualTo(1);
    }

    @Test
    public void testMaxFieldLength() throws IOException {
        writer.close();
        final IndexWriter.MaxFieldLength maxFieldLength = new IndexWriter.MaxFieldLength(1);
        writer = new IndexWriter(directory, new WhitespaceAnalyzer(), maxFieldLength);

        final Document doc = new Document();
        doc.add(new Field("contents", "these bridges can't be found", Store.NO, Index.ANALYZED));

        writer.addDocument(doc);

        assertThat(hitCount("contents", "bridges")).isEqualTo(1);
    }


    private int hitCount(final String fieldName, final String searchString) throws IOException {
        final IndexSearcher searcher = new IndexSearcher(directory);
        final Term term = new Term(fieldName, searchString);
        final Query query = new TermQuery(term);
        final int hitCount = searcher.search(query, 1).totalHits;
        searcher.close();
        return hitCount;
    }
}
