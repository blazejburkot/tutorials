package lucene.analysis;

import lucene.analysis.util.AnalysisToken;
import lucene.analysis.util.AnalyzerUtils;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SimpleAnalyzerTest {
    private SimpleAnalyzer analyzer = new SimpleAnalyzer();

    @Test
    public void test() {
        final String text = "The quick brown fox....";
        final List<AnalysisToken> tokens = AnalyzerUtils.processText(analyzer, text);
        AnalyzerUtils.printTerms(analyzer, text);

        assertThat(tokens).hasSize(4);
        assertThat(tokens.get(0).getTerm()).isEqualTo("the");
        assertThat(tokens.get(1).getTerm()).isEqualTo("quick");
        assertThat(tokens.get(2).getTerm()).isEqualTo("brown");
        assertThat(tokens.get(3).getTerm()).isEqualTo("fox");
    }
}