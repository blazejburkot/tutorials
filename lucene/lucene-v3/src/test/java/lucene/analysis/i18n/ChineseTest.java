package lucene.analysis.i18n;

import lucene.analysis.util.AnalyzerUtils;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.analysis.cn.ChineseAnalyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ChineseTest {

    @Test
    @Ignore
    public void testChinese() throws Exception {
        final Directory directory = IndexBuilder.withBooksData().build();
        final IndexSearcher searcher = new IndexSearcher(directory);
        final Query query = new TermQuery(new Term("title", "\u9053"));

        final TopDocs topDocs = searcher.search(query, 10);

        assertThat(topDocs.totalHits).isEqualTo(1);
    }

    @Test
    public void printChineseTokens() {
        final String chineseLetters = "\u9053\u5fb7\u7d93";
        final Analyzer[] analyzers = {
                new SimpleAnalyzer(),
                new StandardAnalyzer(Version.LUCENE_30),
                new ChineseAnalyzer(),
                new CJKAnalyzer(Version.LUCENE_30),
                new SmartChineseAnalyzer(Version.LUCENE_30)};

        for (Analyzer analyzer : analyzers) {
            AnalyzerUtils.printTerms(analyzer, chineseLetters);
        }
    }
}
