package lucene.analysis;

import lucene.analysis.util.AnalyzerUtils;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordTokenizer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenFilter;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.junit.Test;

import java.io.Reader;

public class NGramFiltersTest {

    @Test
    public void testNGramTokenFilter24() {
        AnalyzerUtils.printTerms(new NGramAnalyzer(), "lettuce", true);
    }

    @Test
    public void testEdgeNGramTokenFilterFront() {
        AnalyzerUtils.printTerms(new FrontEdgeNGramAnalyzer(), "lettuce", true);
    }

    @Test
    public void testEdgeNGramTokenFilterBack() {
        AnalyzerUtils.printTerms(new BackEdgeNGramAnalyzer(), "lettuce", true);
    }


    private static class NGramAnalyzer extends Analyzer {
        public TokenStream tokenStream(String fieldName, Reader reader) {
            return new NGramTokenFilter(new KeywordTokenizer(reader), 2, 4);
        }
    }

    private static class FrontEdgeNGramAnalyzer extends Analyzer {
        public TokenStream tokenStream(String fieldName, Reader reader) {
            return new EdgeNGramTokenFilter(new KeywordTokenizer(reader), EdgeNGramTokenFilter.Side.FRONT, 1, 4);
        }
    }

    private static class BackEdgeNGramAnalyzer extends Analyzer {
        public TokenStream tokenStream(String fieldName, Reader reader) {
            return new EdgeNGramTokenFilter(new KeywordTokenizer(reader), EdgeNGramTokenFilter.Side.BACK, 1, 4);
        }
    }
}
