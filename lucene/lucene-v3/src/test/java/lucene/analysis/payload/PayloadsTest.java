package lucene.analysis.payload;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.payloads.AveragePayloadFunction;
import org.apache.lucene.search.payloads.PayloadTermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class PayloadsTest {
    private Directory directory;
    private IndexSearcher indexSearcher;
    private BulletinPayloadsAnalyzer analyzer;

    @Before
    public void setUp() throws Exception {
        directory = new RAMDirectory();
        analyzer = new BulletinPayloadsAnalyzer(5.0F);

        addDocuments();

        indexSearcher = new IndexSearcher(directory);
        indexSearcher.setSimilarity(new BoostingSimilarity());
    }

    @After
    public void tearDown() throws Exception {
        indexSearcher.close();
        directory.close();
    }

    @Test
    public void testPayloadTermQuery() throws Throwable {
        final Term warning = new Term("contents", "warning");
        final Query termQuery = new TermQuery(warning);
        final TopDocs termQueryResult = indexSearcher.search(termQuery, 10);

        assertThat(termQueryResult.totalHits).isEqualTo(3);
        assertThat(getTitleField(termQueryResult, 0)).isEqualTo("Warning label maker");
        System.out.println("\nTermQuery results:");
        dumpHits(termQueryResult);


        final Query averagePayloadQuery = new PayloadTermQuery(warning, new AveragePayloadFunction());
        final TopDocs averagePayloadQueryResult = indexSearcher.search(averagePayloadQuery, 10);

        assertThat(averagePayloadQueryResult.totalHits).isEqualTo(3);
        assertThat(getTitleField(averagePayloadQueryResult, 0)).isEqualTo("Hurricane warning");
        System.out.println("\nPayloadTermQuery results:");
        dumpHits(averagePayloadQueryResult);
    }

    private String getTitleField(final TopDocs hits, final int idx) throws IOException {
        return indexSearcher.doc(hits.scoreDocs[idx].doc).get("title");
    }


    private void addDocuments() throws IOException {
        final IndexWriter writer = new IndexWriter(directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED);
        addDoc(writer, "Hurricane warning", "Bulletin: A hurricane warning was issued at 6 AM for the outer great banks");
        addDoc(writer, "Warning label maker",
                "The warning label maker is a delightful toy for your precocious seven year old's warning needs");
        addDoc(writer, "Tornado warning", "Bulletin: There is a tornado warning for  Worcester county until 6 PM today");
        writer.close();
    }

    private void addDoc(final IndexWriter writer, String title, String contents) throws IOException {
        Document doc = new Document();
        doc.add(new Field("title", title, Field.Store.YES, Field.Index.NO));
        doc.add(new Field("contents", contents, Field.Store.NO, Field.Index.ANALYZED));
        analyzer.setIsBulletin(contents.startsWith("Bulletin:"));
        writer.addDocument(doc);
    }



    private void dumpHits(TopDocs hits) throws IOException {
        if (hits.totalHits == 0) {
            System.out.println("No hits");
        }

        for (ScoreDoc match : hits.scoreDocs) {
            final Document doc = indexSearcher.doc(match.doc);
            System.out.println(match.score + ":" + doc.get("title"));
        }
    }
}
