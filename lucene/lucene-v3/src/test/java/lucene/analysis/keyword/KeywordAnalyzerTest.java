package lucene.analysis.keyword;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class KeywordAnalyzerTest {
    private IndexSearcher searcher;

    @Before
    public void setUp() throws Exception {
        final Directory directory = IndexBuilder.inMemoryBuilder()
                .withDocument()
                    .withField("partnum", "Q36", Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                    .withField("description", "Illidium Space Modulator", Field.Store.YES, Field.Index.ANALYZED)
                .withAnalyzer(new SimpleAnalyzer())
                .build();

        searcher = new IndexSearcher(directory);
    }

    @Test
    public void testTermQuery() throws Exception {
        final Query query = new TermQuery(new Term("partnum", "Q36"));
        final TopDocs topDocs = searcher.search(query, 10);

        assertThat(topDocs.totalHits).isEqualTo(1);
        assertThat(query.toString()).isEqualTo("partnum:Q36");
    }

    @Test
    public void testBasicQueryParser() throws Exception {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "description", new SimpleAnalyzer());
        final Query query = queryParser.parse("partnum:Q36 AND SPACE");

        final TopDocs topDocs = searcher.search(query, 10);

        assertThat(topDocs.totalHits).isEqualTo(0);
        assertThat(query.toString()).isEqualTo("+partnum:q +description:space");
    }

    @Test
    public void testPerFieldAnalyzer() throws Exception {
        final PerFieldAnalyzerWrapper analyzer = new PerFieldAnalyzerWrapper(new SimpleAnalyzer());
        analyzer.addAnalyzer("partnum", new KeywordAnalyzer());

        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "description", analyzer);
        final Query query = queryParser.parse("partnum:Q36 AND SPACE");

        final TopDocs topDocs = searcher.search(query, 10);

        assertThat(topDocs.totalHits).isEqualTo(1);
        assertThat(query.toString()).isEqualTo("+partnum:Q36 +description:space");
    }
}
