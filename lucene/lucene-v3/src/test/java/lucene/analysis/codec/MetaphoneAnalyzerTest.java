package lucene.analysis.codec;

import lucene.analysis.util.AnalysisToken;
import lucene.analysis.util.AnalyzerUtils;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MetaphoneAnalyzerTest {
    private Analyzer metaphoneAnalyzer = new MetaphoneReplacementAnalyzer();

    private Directory directory = IndexBuilder.inMemoryBuilder()
            .withDocument().withField("contents", "cool cat", Field.Store.YES, Field.Index.ANALYZED)
            .withAnalyzer(metaphoneAnalyzer)
            .build();


    @Test
    public void testKoolKat() throws Exception {
        final IndexSearcher searcher = new IndexSearcher(directory);

        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "contents", metaphoneAnalyzer);
        final Query query = queryParser.parse("kool kat");

        final TopDocs hits = searcher.search(query, 10);
        assertThat(hits.totalHits).isEqualTo(1);

        searcher.close();
    }

    @Test
    public void testKoolKatTokens() {
        // when
        final List<AnalysisToken> koolKatTokens = AnalyzerUtils.processText(metaphoneAnalyzer, "kool kat");
        final List<AnalysisToken> coolCatTokens = AnalyzerUtils.processText(metaphoneAnalyzer, "cool cat");
        // then
        assertThat(koolKatTokens).isEqualTo(coolCatTokens);
        assertThat(coolCatTokens.get(0).getTerm()).isEqualTo("KL");
        assertThat(coolCatTokens.get(0).getType()).isEqualTo("metaphone");
        assertThat(coolCatTokens.get(1).getTerm()).isEqualTo("KT");
        assertThat(coolCatTokens.get(1).getType()).isEqualTo("metaphone");
    }

    @Test
    public void testQuickBrownFoxTokens() {
        // given
        final String quickBrownFoxSentence = "The quick brown fox jumped over the lazy dog";
        final String quikBrownPhoxSentence = "Tha quik brown phox jumpd ovvar tha lazi dag";
        // when
        final List<AnalysisToken> quickBrownFoxTokens = AnalyzerUtils.processText(metaphoneAnalyzer, quickBrownFoxSentence);
        final List<AnalysisToken> quikBrownPhoxTokens = AnalyzerUtils.processText(metaphoneAnalyzer, quikBrownPhoxSentence);
        // then
        assertThat(quickBrownFoxTokens).usingElementComparatorOnFields("term").isEqualTo(quikBrownPhoxTokens);
        AnalyzerUtils.printTerms(metaphoneAnalyzer, quickBrownFoxSentence);
    }
}
