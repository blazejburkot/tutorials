package lucene.analysis.synonym;

import lucene.analysis.util.AnalysisToken;
import lucene.analysis.util.AnalyzerUtils;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class SynonymAnalyzerTest {
    private SynonymAnalyzer synonymAnalyzer = new SynonymAnalyzer(new SynonymEngineStub());
    private Directory directory = IndexBuilder.inMemoryBuilder()
            .withDocument().withField("content", "The quick brown fox jumps over the lazy dog", Field.Store.YES, Field.Index.ANALYZED)
            .withAnalyzer(synonymAnalyzer)
            .build();

    private IndexSearcher searcher;

    @Before
    public void setUp() throws Exception {
        searcher = new IndexSearcher(directory, true);
    }

    @After
    public void tearDown() throws Exception {
        searcher.close();
    }

    @Test
    public void testJumps() {
        // when
        final List<AnalysisToken> tokens = AnalyzerUtils.processText(synonymAnalyzer, "jumps");
        // then
        assertThat(tokens).extracting(AnalysisToken::getTerm).containsExactlyInAnyOrder("jumps", "hops", "leaps");
        assertThat(tokens).extracting(AnalysisToken::getPosition).containsExactlyInAnyOrder(1, 1, 1);
    }

    @Test
    public void testSearchByAPI() throws Exception {
        final TermQuery termQuery = new TermQuery(new Term("content", "hops"));  //#1
        final int totalHits = searcher.search(termQuery, 10).totalHits;
        assertThat(totalHits).isEqualTo(1);

        final PhraseQuery phraseQuery = new PhraseQuery();
        phraseQuery.add(new Term("content", "fox"));
        phraseQuery.add(new Term("content", "hops"));
        final int phraseQueryTotalHits = searcher.search(phraseQuery, 10).totalHits;
        assertThat(phraseQueryTotalHits).isEqualTo(1);
    }

    @Test
    public void testWithQueryParser() throws Exception {
        final String query = "\"fox jumps\"";

        // SYNONYM ANALYZER
        final QueryParser queryParser1 = new QueryParser(Version.LUCENE_30, "content", synonymAnalyzer);
        final Query query1 = queryParser1.parse(query);
        final int totalHits1 = searcher.search(query1, 10).totalHits;
        assertThat(totalHits1).isEqualTo(1);
        System.out.println("Parsed query With SynonymAnalyzer: " + query1.toString());

        // STANDARD ANALYZER
        final QueryParser queryParser2 = new QueryParser(Version.LUCENE_30, "content", new StandardAnalyzer(Version.LUCENE_30));
        final Query query2 = queryParser2.parse(query);
        final int totalHits2 = searcher.search(query2, 10).totalHits;
        assertThat(totalHits2).isEqualTo(1);
        System.out.println("Parsed query With StandardAnalyzer: " + query2.toString());
    }

    @Test
    public void test() {
        final String text = "The quick brown fox jumps over the lazy dog";
        List<AnalysisToken> tokens = AnalyzerUtils.processText(synonymAnalyzer, text);

        AnalyzerUtils.printTerms(synonymAnalyzer, text, true);
        assertThat(tokens).hasSize(16).extracting(AnalysisToken::getPosition).containsOnly(2, 3, 4, 5, 6, 8, 9);
    }
}
