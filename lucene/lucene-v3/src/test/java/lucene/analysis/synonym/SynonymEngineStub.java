package lucene.analysis.synonym;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class SynonymEngineStub implements SynonymEngine {
    private static HashMap<String, List<String>> map = new HashMap<>();

    static {
        map.put("quick", Arrays.asList("fast", "speedy"));
        map.put("jumps", Arrays.asList("leaps", "hops"));
        map.put("over", Collections.singletonList("above"));
        map.put("lazy", Arrays.asList("apathetic", "sluggish"));
        map.put("dog", Arrays.asList("canine", "pooch"));
    }

    @Override
    public List<String> getSynonyms(final String word) {
        return map.getOrDefault(word, Collections.emptyList());
    }
}
