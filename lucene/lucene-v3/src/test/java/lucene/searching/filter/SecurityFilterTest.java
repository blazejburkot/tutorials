package lucene.searching.filter;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class SecurityFilterTest {
    private IndexSearcher searcher;
    private Directory directory;

    @Before
    public void setUp() throws Exception {
        directory = IndexBuilder.inMemoryBuilder()
                .withDocument()
                .withField("owner", "elwood", Field.Store.YES, Field.Index.NOT_ANALYZED)
                .withField("keywords", "elwood's sensitive info", Field.Store.YES, Field.Index.ANALYZED)
                .withDocument()
                .withField("owner", "jake", Field.Store.YES, Field.Index.NOT_ANALYZED)
                .withField("keywords", "jake's sensitive info", Field.Store.YES, Field.Index.ANALYZED)
                .build();
        searcher = new IndexSearcher(directory);
    }

    @After
    public void cleanUp() throws IOException {
        searcher.close();
        directory.close();
    }

    @Test
    public void testSecurityFilter() throws Exception {
        final TermQuery termQuery = new TermQuery(new Term("keywords", "info"));
        final TopDocs searchWithoutSecFilter = searcher.search(termQuery, 10);

        assertThat(searchWithoutSecFilter.totalHits).isEqualTo(2);

        final Filter jakeFilter = new QueryWrapperFilter(new TermQuery(new Term("owner", "jake")));
        final TopDocs searchWithSecFilter = searcher.search(termQuery, jakeFilter, 10);
        final String documentKeywords = searcher.doc(searchWithSecFilter.scoreDocs[0].doc).get("keywords");

        assertThat(searchWithSecFilter.totalHits).isEqualTo(1);
        assertThat(documentKeywords).isEqualTo("jake's sensitive info");
    }
}
