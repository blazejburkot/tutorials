package lucene.searching.filter;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.FilteredQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.Directory;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SpecialsFilterTest {
    private Query allBooksQuery;
    private IndexSearcher searcher;

    @Before
    public void setUp() throws Exception {
        final Directory directory = IndexBuilder.withBooksData().build();
        searcher = new IndexSearcher(directory, true);

        allBooksQuery = new MatchAllDocsQuery();
    }

    @Test
    public void testCustomFilter() throws Exception {
        final String[] isbns = new String[]{"9780061142666", "9780394756820"};
        final Filter filter = new SpecialsFilter(() -> isbns);

        final TopDocs hits = searcher.search(allBooksQuery, filter, 10);

        assertThat(hits.totalHits).isEqualTo(isbns.length);
    }

    @Test
    public void testFilteredQuery() throws Exception {
        final String[] isbns = new String[]{"9780880105118"};
        final Filter filter = new SpecialsFilter(() -> isbns);

        final WildcardQuery educationBooks = new WildcardQuery(new Term("category", "*education*"));
        final FilteredQuery edBooksOnSpecial = new FilteredQuery(educationBooks, filter);
        final TermQuery logoBooks = new TermQuery(new Term("subject", "logo"));

        final BooleanQuery logoOrEdBooks = new BooleanQuery();
        logoOrEdBooks.add(logoBooks, BooleanClause.Occur.SHOULD);
        logoOrEdBooks.add(edBooksOnSpecial, BooleanClause.Occur.SHOULD);

        final TopDocs hits = searcher.search(logoOrEdBooks, 10);

        assertThat(logoOrEdBooks).hasToString("subject:logo filtered(category:*education*)->SpecialsFilter");
        assertThat(hits.totalHits).isEqualTo(1);
    }
}
