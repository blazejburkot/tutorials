package lucene.searching.filter;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.CachingWrapperFilter;
import org.apache.lucene.search.FieldCacheRangeFilter;
import org.apache.lucene.search.FieldCacheTermsFilter;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.NumericRangeFilter;
import org.apache.lucene.search.PrefixFilter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.SpanQueryFilter;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeFilter;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.spans.SpanQuery;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class FilterTest {
    private Query allBooksQuery;
    private IndexSearcher searcher;
    private Directory dir;

    @Before
    public void setUp() throws Exception {
        allBooksQuery = new MatchAllDocsQuery();
        dir = IndexBuilder.withBooksData().build();
        searcher = new IndexSearcher(dir);
    }

    @After
    public void tearDown() throws Exception {
        searcher.close();
        dir.close();
    }

    @Test
    public void testTermRangeFilter() throws Exception {
        final Filter filter = new TermRangeFilter("title2", "d", "j", true, true);

        assertNumberOfTotalHits(filter, 3);
    }

    @Test
    public void testNumericDateFilter() throws Exception {
        // pub date of Lucene in Action, Second Edition and JUnit in Action, Second Edition is May 2010
        final Filter filter = NumericRangeFilter.newIntRange("pubmonth", 201001, 201006, true, true);

        assertNumberOfTotalHits(filter, 2);
    }

    @Test
    public void testFieldCacheRangeFilter() throws Exception {
        final Filter stringRangeFilter = FieldCacheRangeFilter.newStringRange("title2", "d", "j", true, true);
        assertNumberOfTotalHits(stringRangeFilter, 3);

        final Filter intRangeFilter = FieldCacheRangeFilter.newIntRange("pubmonth", 201001, 201006, true, true);
        assertNumberOfTotalHits(intRangeFilter, 2);
    }

    @Test
    public void testFieldCacheTermsFilter() throws Exception {
        final Filter filter = new FieldCacheTermsFilter("category", "/health/alternative/chinese", "/technology/computers/ai");

        assertNumberOfTotalHits(filter, 2);
    }

    @Test
    public void testQueryWrapperFilter() throws Exception {
        final TermQuery categoryQuery = new TermQuery(new Term("category", "/philosophy/eastern"));
        final Filter categoryFilter = new QueryWrapperFilter(categoryQuery);

        assertNumberOfTotalHits(categoryFilter, 1);
    }

    @Test
    public void testSpanQueryFilter() throws Exception {
        final SpanQuery categoryQuery = new SpanTermQuery(new Term("category", "/philosophy/eastern"));
        final Filter categoryFilter = new SpanQueryFilter(categoryQuery);

        assertNumberOfTotalHits(categoryFilter, 1);
    }

    @Test
    public void testFilterAlternative() throws Exception {
        final TermQuery categoryQuery = new TermQuery(new Term("category", "/philosophy/eastern"));

        final BooleanQuery constrainedQuery = new BooleanQuery();
        constrainedQuery.add(allBooksQuery, BooleanClause.Occur.MUST);
        constrainedQuery.add(categoryQuery, BooleanClause.Occur.MUST);

        final TopDocs topDocs = searcher.search(constrainedQuery, 10);
        assertThat(topDocs.totalHits).isEqualTo(1);
    }

    @Test
    public void testPrefixFilter() throws Exception {
        final Filter prefixFilter = new PrefixFilter(new Term("category", "/technology/computers"));

        assertNumberOfTotalHits(prefixFilter, 8);
    }

    @Test
    public void testCachingWrapper() throws Exception {
        final Filter filter = new TermRangeFilter("title2", "d", "j", true, true);
        final CachingWrapperFilter cachingFilter = new CachingWrapperFilter(filter);

        assertNumberOfTotalHits(cachingFilter, 3);
    }

    private void assertNumberOfTotalHits(final Filter filter, final int expectedTotalHits) throws IOException {
        final TopDocs topDocs = searcher.search(allBooksQuery, filter, 10);
        assertThat(topDocs.totalHits).isEqualTo(expectedTotalHits);
    }
}
