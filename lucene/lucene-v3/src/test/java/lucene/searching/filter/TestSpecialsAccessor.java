package lucene.searching.filter;

public class TestSpecialsAccessor implements SpecialsFilter.SpecialsAccessor {
  private String[] isbns;

  public TestSpecialsAccessor(String[] isbns) {
    this.isbns = isbns;
  }

  public String[] isbns() {
    return isbns;
  }
}
