package lucene.searching.queryparser;

import junit.framework.TestCase;

import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.util.Version;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class CustomQueryParserTest extends TestCase {
    private CustomQueryParser customQueryParser = new CustomQueryParser(Version.LUCENE_30, "field", new WhitespaceAnalyzer());

    @Test
    public void testCustomQueryParser() {
        assertThatExceptionOfType(ParseException.class)
                .as("Wildcard queries should not be allowed")
                .isThrownBy(() -> customQueryParser.parse("a?t"));

        assertThatExceptionOfType(ParseException.class)
                .as("Fuzzy queries should not be allowed")
                .isThrownBy(() -> customQueryParser.parse("xunit~"));
    }

    public void testPhraseQuery() throws Exception {
        final Query termQuery = customQueryParser.parse("singleTerm");
        assertThat(termQuery).isInstanceOf(TermQuery.class).hasToString("field:singleTerm");

        final Query spanQuery = customQueryParser.parse("\"a phrase\"");
        assertThat(spanQuery).isInstanceOf(SpanNearQuery.class).hasToString("spanNear([field:a, field:phrase], 0, true)");
    }
}
