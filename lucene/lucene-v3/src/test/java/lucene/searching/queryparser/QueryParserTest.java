package lucene.searching.queryparser;

import lucene.util.TestUtil;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class QueryParserTest {

    private static Directory dir;

    private static Analyzer analyzer;
    private static IndexSearcher searcher;

    @BeforeClass
    public static void setUp() throws Exception {
        analyzer = new WhitespaceAnalyzer();
        dir = IndexBuilder.withBooksData().build();
        searcher = new IndexSearcher(dir);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        searcher.close();
        dir.close();
    }

    @Test
    public void testToString() {
        final BooleanQuery query = new BooleanQuery();
        query.add(new FuzzyQuery(new Term("field", "kountry")), BooleanClause.Occur.MUST);
        query.add(new TermQuery(new Term("title", "western")), BooleanClause.Occur.SHOULD);

        assertThat(query.toString("field")).isEqualTo("+kountry~0.5 title:western");
        assertThat(query.toString("title")).isEqualTo("+field:kountry~0.5 western");
        assertThat(query.toString("")).isEqualTo("+field:kountry~0.5 title:western");
        assertThat(query.toString("alaMaKota")).isEqualTo("+field:kountry~0.5 title:western");
        assertThat(query.toString()).isEqualTo("+field:kountry~0.5 title:western");
    }

    @Test
    public void testPrefixQuery() throws Exception {
        final QueryParser parser = new QueryParser(Version.LUCENE_30, "category", new StandardAnalyzer(Version.LUCENE_30));
        parser.setLowercaseExpandedTerms(false);

        final Query query = parser.parse("/Computers/technology*");

        assertThat(query.toString("category")).isEqualTo("/Computers/technology*");
    }

    @Test
    public void testFuzzyQuery() throws Exception {
        final QueryParser parser = new QueryParser(Version.LUCENE_30, "subject", analyzer);
        final Query query = parser.parse("kountry~");
        assertThat(query.toString()).isEqualTo("subject:kountry~0.5");

        final Query query2 = parser.parse("kountry~0.7");
        assertThat(query2.toString()).isEqualTo("subject:kountry~0.7");
    }

    @Test
    public void testGrouping() throws Exception {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "subject", analyzer);
        final BooleanQuery query = (BooleanQuery) queryParser.parse("(agile OR extreme) AND methodology");
        final TopDocs matches = searcher.search(query, 10);

        assertThat(TestUtil.hitsIncludeTitle(searcher, matches, "Extreme Programming Explained")).isTrue();
        assertThat(query.getClauses()).hasSize(2);
        assertThat(query.getClauses()[0].toString()).isEqualTo("+subject:agile subject:extreme");
        assertThat(query.getClauses()[1].toString()).isEqualTo("+subject:methodology");
    }

    @Test
    public void testTermQuery() throws Exception {
        final QueryParser parser = new QueryParser(Version.LUCENE_30, "subject", analyzer);
        final Query query = parser.parse("computers");

        assertThat(query).isInstanceOf(TermQuery.class);
        assertThat(query.toString()).isEqualTo("subject:computers");
    }

    @Test
    public void testTermRangeQuery() throws Exception {
        QueryParser queryParser = new QueryParser(Version.LUCENE_30, "f", analyzer);
        final TermRangeQuery inclusiveRangeQuery = (TermRangeQuery) queryParser.parse("t:[Q TO V]");
        assertThat(inclusiveRangeQuery.getLowerTerm()).isEqualTo("q");
        assertThat(inclusiveRangeQuery.getUpperTerm()).isEqualTo("v");
        assertThat(inclusiveRangeQuery.includesLower()).isTrue();
        assertThat(inclusiveRangeQuery.includesUpper()).isTrue();


        final TermRangeQuery exclusiveRangeQuery = (TermRangeQuery) queryParser.parse("t:{q TO v}");
        assertThat(exclusiveRangeQuery.getLowerTerm()).isEqualTo("q");
        assertThat(exclusiveRangeQuery.getUpperTerm()).isEqualTo("v");
        assertThat(exclusiveRangeQuery.includesLower()).isFalse();
        assertThat(exclusiveRangeQuery.includesUpper()).isFalse();
    }

    @Test
    public void testPhraseQuery() throws Exception {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "field", new StandardAnalyzer(Version.LUCENE_30));
        final Query query = queryParser.parse("\"This is Some Phrase*\"");
        assertThat(query.toString("field")).isEqualTo("\"? ? some phrase\"");

        final Query termQuery = new QueryParser(Version.LUCENE_30, "field", analyzer).parse("\"term\"");
        assertThat(termQuery).isInstanceOf(TermQuery.class);
    }

    @Test
    public void testSlop() throws Exception {
        final QueryParser defaultQueryParser = new QueryParser(Version.LUCENE_30, "field", analyzer);
        final Query query = defaultQueryParser.parse("\"exact phrase\"");
        assertThat(query.toString("field")).isEqualTo("\"exact phrase\"");

        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "field", analyzer);
        queryParser.setPhraseSlop(5);
        final Query query2 = queryParser.parse("\"sloppy phrase\"");
        assertThat(query2.toString("field")).isEqualTo("\"sloppy phrase\"~5");
    }

    @Test
    public void testLowercasing() throws Exception {
        final QueryParser defaultQueryParser = new QueryParser(Version.LUCENE_30, "field", analyzer);
        final Query queryLowercased = defaultQueryParser.parse("PrefixQuery*");
        assertThat(queryLowercased.toString("field")).isEqualTo("prefixquery*");

        final QueryParser customQueryParser = new QueryParser(Version.LUCENE_30, "field", analyzer);
        customQueryParser.setLowercaseExpandedTerms(false);
        final Query query = customQueryParser.parse("PrefixQuery*");
        assertThat(query.toString("field")).isEqualTo("PrefixQuery*");
    }

    @Test
    public void testWildcard() {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "field", analyzer);
        assertThatExceptionOfType(ParseException.class)
                .isThrownBy(() -> queryParser.parse("*xyz"));
    }

    @Test
    public void testBoost() throws Exception {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "field", analyzer);
        final Query query = queryParser.parse("term^2");
        assertThat(query.toString("field")).isEqualTo("term^2.0");
    }

    @Test
    public void testParseException() {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "contents", analyzer);
        assertThatExceptionOfType(ParseException.class)
                .isThrownBy(() -> queryParser.parse("^&#"));
    }
}
