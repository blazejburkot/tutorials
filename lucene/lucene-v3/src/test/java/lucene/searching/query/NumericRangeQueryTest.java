package lucene.searching.query;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NumericRangeQueryTest {

    @Test
    public void testInclusive() throws Exception {
        Directory dir = IndexBuilder.withBooksData().build();
        IndexSearcher searcher = new IndexSearcher(dir);
        // pub date of TTC was September 2006
        NumericRangeQuery query = NumericRangeQuery.newIntRange("pubmonth", 200605, 200609, true, true);

        TopDocs matches = searcher.search(query, 10);

        assertThat(matches.totalHits).isEqualTo(1);
        for (int i=0; i<matches.totalHits; i++) {
            final Document doc = searcher.doc(matches.scoreDocs[i].doc);
            assertThat(Integer.parseInt(doc.get("pubmonth"))).isGreaterThanOrEqualTo(200605).isLessThanOrEqualTo(200609);
        }

        searcher.close();
        dir.close();
    }

    @Test
    public void testExclusive() throws Exception {
        Directory dir = IndexBuilder.withBooksData().build();
        IndexSearcher searcher = new IndexSearcher(dir);

        // pub date of TTC was September 2006
        NumericRangeQuery query = NumericRangeQuery.newIntRange("pubmonth", 200605, 200609, false, false);
        TopDocs matches = searcher.search(query, 10);

        assertThat(matches.totalHits).isEqualTo(0);

        searcher.close();
        dir.close();
    }
}
