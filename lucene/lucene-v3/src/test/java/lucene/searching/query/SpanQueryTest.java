package lucene.searching.query;

import lucene.analysis.util.AnalyzerUtils;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SpanQueryFilter;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.spans.SpanFirstQuery;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.search.spans.SpanNotQuery;
import org.apache.lucene.search.spans.SpanOrQuery;
import org.apache.lucene.search.spans.SpanQuery;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.search.spans.Spans;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class SpanQueryTest {
    private Directory directory;
    private IndexSearcher searcher;
    private IndexReader reader;
    private Analyzer analyzer;

    private SpanTermQuery quickQuery;
    private SpanTermQuery brownQuery;
    private SpanTermQuery redQuery;
    private SpanTermQuery foxQuery;
    private SpanTermQuery lazyQuery;
    private SpanTermQuery sleepyQuery;
    private SpanTermQuery dogQuery;
    private SpanTermQuery catQuery;

    @Before
    public void setUp() throws Exception {
        analyzer = new WhitespaceAnalyzer();

        directory = IndexBuilder.inMemoryBuilder()
                .withDocument().withField("f", "the quick brown fox jumps over the lazy dog", Field.Store.YES, Field.Index.ANALYZED)
                .withDocument().withField("f", "the quick red fox jumps over the sleepy cat", Field.Store.YES, Field.Index.ANALYZED)
                .withAnalyzer(analyzer)
                .build();

        searcher = new IndexSearcher(directory);
        reader = searcher.getIndexReader();

        quickQuery = new SpanTermQuery(new Term("f", "quick"));
        brownQuery = new SpanTermQuery(new Term("f", "brown"));
        redQuery = new SpanTermQuery(new Term("f", "red"));
        foxQuery = new SpanTermQuery(new Term("f", "fox"));
        lazyQuery = new SpanTermQuery(new Term("f", "lazy"));
        sleepyQuery = new SpanTermQuery(new Term("f", "sleepy"));
        dogQuery = new SpanTermQuery(new Term("f", "dog"));
        catQuery = new SpanTermQuery(new Term("f", "cat"));
    }

    @After
    public void cleanUp() throws IOException {
        this.searcher.close();
        this.directory.close();
    }

    @Test
    public void testSpanTermQuery() throws Exception {
        dumpSpans(brownQuery);
        assertThatOnlyBrownFoxIsPresent(brownQuery);
    }

    @Test
    public void testSpanFirstQuery() throws Exception {
        final SpanFirstQuery spanFirstQueryEnd2 = new SpanFirstQuery(brownQuery, 2);
        dumpSpans(spanFirstQueryEnd2);
        assertThatThereIsNoResults(spanFirstQueryEnd2);

        final SpanFirstQuery spanFirstQueryEnd3 = new SpanFirstQuery(brownQuery, 3);
        dumpSpans(spanFirstQueryEnd3);
        assertThatOnlyBrownFoxIsPresent(spanFirstQueryEnd3);
    }

    @Test
    public void testSpanNearQuery() throws Exception {
        final SpanQuery[] quickBrownDogQuery = new SpanQuery[]{quickQuery, brownQuery, dogQuery};

        final SpanNearQuery spanNearQuerySlop0 = new SpanNearQuery(quickBrownDogQuery, 0, true);
        dumpSpans(spanNearQuerySlop0);
        assertThatThereIsNoResults(spanNearQuerySlop0);

        final SpanNearQuery spanNearQuerySlop4 = new SpanNearQuery(quickBrownDogQuery, 4, true);
        dumpSpans(spanNearQuerySlop4);
        assertThatThereIsNoResults(spanNearQuerySlop4);

        final SpanNearQuery spanNearQuerySlop5 = new SpanNearQuery(quickBrownDogQuery, 5, true);
        dumpSpans(spanNearQuerySlop5);
        assertThatOnlyBrownFoxIsPresent(spanNearQuerySlop5);

        // interesting - even a sloppy phrase query would require more slop to match
        final SpanQuery[] lazyFoxQuery = {lazyQuery, foxQuery};
        final SpanNearQuery spanNearQuerySlop3 = new SpanNearQuery(lazyFoxQuery, 3, false); // Nested SpanTermQuery objects in reverse order
        dumpSpans(spanNearQuerySlop3);
        assertThatOnlyBrownFoxIsPresent(spanNearQuerySlop3);

        final PhraseQuery phraseQuery = new PhraseQuery();
        phraseQuery.add(new Term("f", "lazy"));
        phraseQuery.add(new Term("f", "fox"));
        phraseQuery.setSlop(4);
        assertThatThereIsNoResults(phraseQuery);

        phraseQuery.setSlop(5);
        assertThatOnlyBrownFoxIsPresent(phraseQuery);
    }

    @Test
    public void testSpanQueryFilter() throws Exception {
        final SpanQuery[] quickBrownDogQuery = new SpanQuery[]{quickQuery, brownQuery, dogQuery};
        final SpanQuery spanNearQuery = new SpanNearQuery(quickBrownDogQuery, 5, true);
        final Filter spanQueryFilter = new SpanQueryFilter(spanNearQuery);

        final Query query = new MatchAllDocsQuery();
        final TopDocs hits = searcher.search(query, spanQueryFilter, 10);

        assertThat(hits.totalHits).isEqualTo(1);
        assertThat(hits.scoreDocs[0].doc).isEqualTo(0);
    }

    @Test
    public void testSpanNotQuery() throws Exception {
        final SpanNearQuery quickFoxQuery = new SpanNearQuery(new SpanQuery[]{quickQuery, foxQuery}, 1, true);
        dumpSpans(quickFoxQuery);
        assertThatBothFoxesIsPresent(quickFoxQuery);

        final SpanNotQuery quickFoxDogQuery = new SpanNotQuery(quickFoxQuery, dogQuery);
        assertThatBothFoxesIsPresent(quickFoxDogQuery);
        dumpSpans(quickFoxDogQuery);

        SpanNotQuery noQuickRedFoxQuery = new SpanNotQuery(quickFoxQuery, redQuery);
        dumpSpans(noQuickRedFoxQuery);
        assertThatOnlyBrownFoxIsPresent(noQuickRedFoxQuery);
    }

    @Test
    public void testSpanOrQuery() throws Exception {
        final SpanNearQuery quickFoxQuery = new SpanNearQuery(new SpanQuery[]{quickQuery, foxQuery}, 1, true);
        final SpanNearQuery lazyDogQuery = new SpanNearQuery(new SpanQuery[]{lazyQuery, dogQuery}, 0, true);
        final SpanNearQuery sleepyCatQuery = new SpanNearQuery(new SpanQuery[]{sleepyQuery, catQuery}, 0, true);

        final SpanNearQuery quickFoxNearToLazyDogQuery = new SpanNearQuery(new SpanQuery[]{quickFoxQuery, lazyDogQuery}, 3, true);
        dumpSpans(quickFoxNearToLazyDogQuery);
        assertThatOnlyBrownFoxIsPresent(quickFoxNearToLazyDogQuery);

        final SpanNearQuery quickFoxNearToSleepyCatQuery = new SpanNearQuery(new SpanQuery[]{quickFoxQuery, sleepyCatQuery}, 3, true);
        dumpSpans(quickFoxNearToSleepyCatQuery);

        final SpanOrQuery spanOrQuery = new SpanOrQuery(quickFoxNearToLazyDogQuery, quickFoxNearToSleepyCatQuery);
        dumpSpans(spanOrQuery);
        assertThatBothFoxesIsPresent(spanOrQuery);
    }

    @Test
    public void testPlay() throws Exception {
        final SpanOrQuery spanOrQuery = new SpanOrQuery(quickQuery, foxQuery);
        dumpSpans(spanOrQuery);

        final SpanNearQuery quickFoxQuery = new SpanNearQuery(new SpanQuery[]{quickQuery, foxQuery}, 1, true);
        final SpanFirstQuery spanFirstQuery = new SpanFirstQuery(quickFoxQuery, 4);
        dumpSpans(spanFirstQuery);

        dumpSpans(new SpanTermQuery(new Term("f", "the")));

        final SpanNearQuery quickBrownQuery = new SpanNearQuery(new SpanQuery[]{quickQuery, brownQuery}, 0, false);
        dumpSpans(quickBrownQuery);
    }

    private void dumpSpans(final SpanQuery query) throws IOException {
        final TopDocs hits = searcher.search(query, 10);

        final Spans spans = query.getSpans(reader);
        System.out.println("query -> " + query);
        int numSpans = 0;

        while (spans.next()) {
            numSpans++;

            final int docNum = spans.doc();
            final String fieldValue = reader.document(docNum).get("f");
            final float score = getById(hits, docNum).score;

            final String line = String.format("   %s (%s) ", AnalyzerUtils.markSpanTerm(analyzer, fieldValue, spans.start(), spans.end()), score);
            System.out.println(line);
        }

        if (numSpans == 0) {
            System.out.println("   No spans");
        }
        System.out.println();
    }

    private ScoreDoc getById(final TopDocs hits, final int docNum) {
        for (ScoreDoc scoreDoc : hits.scoreDocs) {
            if (scoreDoc.doc == docNum) {
                return scoreDoc;
            }
        }
        throw new IllegalArgumentException("cannot find document with number " + docNum);

    }


    private void assertThatOnlyBrownFoxIsPresent(final Query query) throws Exception {
        final TopDocs hits = searcher.search(query, 10);
        assertThat(hits.totalHits).isEqualTo(1);
        assertThat(hits.scoreDocs[0].doc).isEqualTo(0);
    }

    private void assertThatBothFoxesIsPresent(final Query query) throws Exception {
        final TopDocs hits = searcher.search(query, 10);
        assertThat(hits.totalHits).isEqualTo(2);
    }

    private void assertThatThereIsNoResults(final Query query) throws Exception {
        final TopDocs hits = searcher.search(query, 10);
        assertThat(hits.totalHits).isEqualTo(0);
    }
}
