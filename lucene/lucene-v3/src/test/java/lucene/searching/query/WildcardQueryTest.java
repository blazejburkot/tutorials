package lucene.searching.query;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WildcardQueryTest {
    private Directory directory;

    @Before
    public void setUp() {
        directory = IndexBuilder.inMemoryBuilder()
            .withDocument().withField("contents", "wild", Field.Store.YES, Field.Index.ANALYZED)
            .withDocument().withField("contents", "child", Field.Store.YES, Field.Index.ANALYZED)
            .withDocument().withField("contents", "mild", Field.Store.YES, Field.Index.ANALYZED)
            .withDocument().withField("contents", "mildew", Field.Store.YES, Field.Index.ANALYZED)
            .build();
    }

    @After
    public void tearDown() throws Exception {
        directory.close();
    }

    @Test
    public void testWildcard() throws Exception {
        final IndexSearcher searcher = new IndexSearcher(directory);
        final WildcardQuery wildcardQuery = new WildcardQuery(new Term("contents", "?ild*"));
        final TopDocs matches = searcher.search(wildcardQuery, 10);

        assertThat(matches.totalHits).isEqualTo(3);
        assertThat(matches.scoreDocs[0].score).isEqualTo(matches.scoreDocs[1].score);
        assertThat(matches.scoreDocs[1].score).isEqualTo(matches.scoreDocs[2].score);

        searcher.close();
    }
}
