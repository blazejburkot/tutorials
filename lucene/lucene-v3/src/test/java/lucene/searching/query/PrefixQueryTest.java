package lucene.searching.query;

import junit.framework.TestCase;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PrefixQueryTest extends TestCase {

    @Test
    public void testPrefixQuery() throws Exception {
        final Directory dir = IndexBuilder.withBooksData().build();
        final IndexSearcher searcher = new IndexSearcher(dir);

        final String searchPrefix = "/technology/computers";
        final Term term = new Term("category", searchPrefix);
        final PrefixQuery prefixQuery = new PrefixQuery(term);

        final TopDocs prefixQueryMatches = searcher.search(prefixQuery, 10);
        final TopDocs termQueryMatches = searcher.search(new TermQuery(term), 10);

        assertThat(prefixQueryMatches.totalHits).isGreaterThan(termQueryMatches.totalHits);
        for (int idx=0; idx<prefixQueryMatches.totalHits; ++idx) {
            final Document doc = searcher.doc(prefixQueryMatches.scoreDocs[idx].doc);
            assertThat(doc.get("category")).startsWith(searchPrefix);
        }

        searcher.close();
        dir.close();
    }
}
