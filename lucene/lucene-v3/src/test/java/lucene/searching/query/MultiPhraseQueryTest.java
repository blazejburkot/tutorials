package lucene.searching.query;

import lucene.analysis.synonym.SynonymAnalyzer;
import lucene.analysis.synonym.SynonymEngine;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.Before;
import org.junit.Test;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

public class MultiPhraseQueryTest {
    private IndexSearcher searcher;

    @Before
    public void setUp() throws Exception {
        final Directory directory = IndexBuilder.inMemoryBuilder()
                .withDocument().withField("field", "the quick brown fox jumped over the lazy dog", Field.Store.YES, Field.Index.ANALYZED)
                .withDocument().withField("field", "the fast fox hopped over the hound", Field.Store.YES, Field.Index.ANALYZED)
                .withAnalyzer(new WhitespaceAnalyzer())
                .build();

        searcher = new IndexSearcher(directory);
    }

    @Test
    public void testBasic() throws Exception {
        final MultiPhraseQuery query = new MultiPhraseQuery();
        query.add(new Term[]{ // Any of these terms may be in first position to match
                new Term("field", "quick"),
                new Term("field", "fast")
        });
        query.add(new Term("field", "fox"));

        final TopDocs result1 = searcher.search(query, 10);

        assertThat(query.toString()).isEqualTo("field:\"(quick fast) fox\"");
        assertThat(result1.totalHits).isEqualTo(1);


        query.setSlop(1);
        final TopDocs result2 = searcher.search(query, 10);

        assertThat(query.toString()).isEqualTo("field:\"(quick fast) fox\"~1");
        assertThat(result2.totalHits).isEqualTo(2);
    }

    @Test
    public void testAgainstOR() throws Exception {
        final PhraseQuery quickFox = new PhraseQuery();
        quickFox.setSlop(1);
        quickFox.add(new Term("field", "quick"));
        quickFox.add(new Term("field", "fox"));

        final PhraseQuery fastFox = new PhraseQuery();
        fastFox.add(new Term("field", "fast"));
        fastFox.add(new Term("field", "fox"));

        final BooleanQuery query = new BooleanQuery();
        query.add(quickFox, BooleanClause.Occur.SHOULD);
        query.add(fastFox, BooleanClause.Occur.SHOULD);

        final TopDocs hits = searcher.search(query, 10);

        assertThat(query.toString()).isEqualTo("field:\"quick fox\"~1 field:\"fast fox\"");
        assertThat(hits.totalHits).isEqualTo(2);
    }

    @Test
    public void testQueryParser() throws Exception {
        final SynonymEngine engine = s -> s.equals("quick") ? singletonList("fast") : emptyList();

        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "field", new SynonymAnalyzer(engine));
        final Query query = queryParser.parse("\"quick fox\"");

        assertThat(query.toString()).isEqualTo("field:\"(quick fast) fox\"");
        assertThat(query).isInstanceOf(MultiPhraseQuery.class);
    }
}