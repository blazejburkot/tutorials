package lucene.searching.query;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FuzzySearchTest {

    @Test
    public void testFuzzy() throws Exception {
        final Directory directory = IndexBuilder.inMemoryBuilder()
                .withDocument().withField("contents", "fuzzy", Field.Store.YES, Field.Index.ANALYZED)
                .withDocument().withField("contents", "wuzzy", Field.Store.YES, Field.Index.ANALYZED)
                .build();
        final IndexSearcher searcher = new IndexSearcher(directory);

        final FuzzyQuery fuzzyQuery = new FuzzyQuery(new Term("contents", "wuzza"));
        final TopDocs matches = searcher.search(fuzzyQuery, 10);

        assertThat(matches.totalHits).isEqualTo(2);
        assertThat(matches.scoreDocs[0].score).isGreaterThan(matches.scoreDocs[1].score);

        final Document doc0 = searcher.doc(matches.scoreDocs[0].doc);
        assertThat(doc0.get("contents")).isEqualTo("wuzzy");

        searcher.close();
        directory.close();
    }
}
