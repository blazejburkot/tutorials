package lucene.searching.query;

import lucene.util.TestUtil;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class MultiFieldQueryParserTest {
    private Directory directory;
    private IndexSearcher indexSearcher;
    private SimpleAnalyzer analyzer;

    @Before
    public void setUp() throws IOException {
        this.analyzer = new SimpleAnalyzer();
        this.directory = IndexBuilder.withBooksData().withAnalyzer(analyzer).build();
        this.indexSearcher = new IndexSearcher(directory, true);
    }

    @After
    public void cleanUp() throws IOException {
        indexSearcher.close();
        directory.close();
    }

    @Test
    public void testDefaultOperator() throws Exception {
        final MultiFieldQueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_30,
                new String[]{"title", "subject"},
                analyzer);

        final Query query = queryParser.parse("development");
        final TopDocs hits = indexSearcher.search(query, 10);

        assertThat(query.toString()).isEqualTo("title:development subject:development");
        assertThat(hits.totalHits).isEqualTo(2);
        assertThat(TestUtil.hitsIncludeTitle(indexSearcher, hits, "Ant in Action")).isTrue();
        assertThat(TestUtil.hitsIncludeTitle(indexSearcher, hits, "Extreme Programming Explained")).isTrue();

    }

    @Test
    public void testSpecifiedOperator() throws Exception {
        final Query query = MultiFieldQueryParser.parse(Version.LUCENE_30, "lucene",
                new String[]{"title", "subject"},
                new BooleanClause.Occur[]{BooleanClause.Occur.MUST, BooleanClause.Occur.MUST},
                analyzer);

        final TopDocs hits = indexSearcher.search(query, 10);

        assertThat(hits.totalHits).isEqualTo(1);
        assertThat(TestUtil.hitsIncludeTitle(indexSearcher, hits, "Lucene in Action, Second Edition")).isTrue();
    }
}
