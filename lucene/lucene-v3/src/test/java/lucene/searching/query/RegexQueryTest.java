package lucene.searching.query;

import lucene.util.TestUtil;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.regex.RegexQuery;
import org.apache.lucene.store.Directory;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RegexQueryTest {

    @Test
    public void testRegexQuery() throws Exception {
        final Directory directory = IndexBuilder.withBooksData().build();
        final IndexSearcher searcher = new IndexSearcher(directory);
        final RegexQuery regexQuery = new RegexQuery(new Term("title", ".*st.*"));

        final TopDocs hits = searcher.search(regexQuery, 10);

        assertThat(hits.totalHits).isEqualTo(2);
        assertThat(TestUtil.hitsIncludeTitle(searcher, hits, "Tapestry in Action")).isTrue();
        assertThat(TestUtil.hitsIncludeTitle(searcher, hits, "Mindstorms: Children, Computers, And Powerful Ideas")).isTrue();

        searcher.close();
        directory.close();
    }
}
