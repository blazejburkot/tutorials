package lucene.searching.query;

import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class PhraseQueryTest {
    private Directory dir;
    private IndexSearcher searcher;

    @Before
    public void setUp() throws IOException {
        dir = new RAMDirectory();
        final IndexWriter writer = new IndexWriter(dir, new WhitespaceAnalyzer(), IndexWriter.MaxFieldLength.UNLIMITED);
        final Document doc = new Document();
        doc.add(new Field("field", "the quick brown fox jumped over the lazy dog", Field.Store.YES, Field.Index.ANALYZED));
        writer.addDocument(doc);
        writer.close();

        searcher = new IndexSearcher(dir);
    }

    @After
    public void tearDown() throws IOException {
        searcher.close();
        dir.close();
    }


    @Test
    public void testSlopComparison() throws Exception {
        final String[] phrase = {"quick", "fox"};

        assertThat(matched(0, phrase)).withFailMessage("exact phrase not found").isFalse();
        assertThat(matched(1, phrase)).withFailMessage("should found document").isTrue();
        assertThat(matched(3, phrase)).withFailMessage("should found document").isTrue();
    }

    @Test
    public void testReverse() throws Exception {
        final String[] phrase = {"fox", "quick"};

        assertThat(matched(2, phrase)).isFalse();
        assertThat(matched(3, phrase)).isTrue();
    }

    @Test
    public void testMultiple() throws Exception {
        assertThat(matched(3, "quick", "jumped", "lazy")).isFalse();
        assertThat(matched(4, "quick", "jumped", "lazy")).isTrue();
        assertThat(matched(7, "lazy", "jumped", "quick")).isFalse();
        assertThat(matched(8, "lazy", "jumped", "quick")).isTrue();
    }

    private boolean matched(int slop, String ... phrase) throws IOException {
        final PhraseQuery phraseQuery = new PhraseQuery();
        phraseQuery.setSlop(slop); // set the maximum allowable positional distance between terms
        Stream.of(phrase).forEach(word -> phraseQuery.add(new Term("field", word)));

        final TopDocs matches = searcher.search(phraseQuery, 10);
        return matches.totalHits > 0;
    }
}
