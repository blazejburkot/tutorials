package lucene.searching.query;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TermRangeQueryTest {

    @Test
    public void testTermRangeQuery() throws Exception {
        final Directory dir = IndexBuilder.withBooksData().build();
        final IndexSearcher searcher = new IndexSearcher(dir);
        final TermRangeQuery query = new TermRangeQuery("title2", "d", "j", true, true);

        final TopDocs matches = searcher.search(query, 100);

        assertThat(matches.totalHits).isEqualTo(3);
        for(int i=0; i<matches.totalHits; i++) {
            final Document doc = searcher.doc(matches.scoreDocs[i].doc);
            assertThat(doc.get("title2").charAt(0)).isGreaterThanOrEqualTo('d').isLessThanOrEqualTo('j');
        }

        searcher.close();
        dir.close();
    }
}
