package lucene.searching;

import lucene.util.TestUtil;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.junit.Test;

import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;

public class HighlightTest {

    @Test
    public void testHighlighting() throws Exception {
        final String text = "The quick brown fox jumps over the lazy dog";
        final TermQuery query = new TermQuery(new Term("field", "fox"));

        final TokenStream tokenStream = new SimpleAnalyzer().tokenStream("field", new StringReader(text));

        final QueryScorer scorer = new QueryScorer(query, "field");
        final Fragmenter fragmenter = new SimpleSpanFragmenter(scorer);
        final Highlighter highlighter = new Highlighter(scorer);
        highlighter.setTextFragmenter(fragmenter);

        final String highlighterBestFragment = highlighter.getBestFragment(tokenStream, text);

        assertThat(highlighterBestFragment).isEqualTo("The quick brown <B>fox</B> jumps over the lazy dog");
    }

    @Test
    public void testHits() throws Exception {
        final Analyzer analyzer = new SimpleAnalyzer();
        final Directory directory = IndexBuilder.withBooksData().withAnalyzer(analyzer).build();
        final IndexSearcher searcher = new IndexSearcher(directory);
        final TermQuery query = new TermQuery(new Term("title", "action"));
        final TopDocs hits = searcher.search(query, 10);

        final QueryScorer scorer = new QueryScorer(query, "title");
        final Highlighter highlighter = new Highlighter(scorer);
        highlighter.setTextFragmenter(new SimpleSpanFragmenter(scorer));


        for (ScoreDoc sd : hits.scoreDocs) {
            final Document doc = searcher.doc(sd.doc);
            final String title = doc.get("title");

            final TokenStream stream = TokenSources.getAnyTokenStream(searcher.getIndexReader(), sd.doc, "title", doc, analyzer);
            final String fragment = highlighter.getBestFragment(stream, title);

            System.out.println(fragment);
        }
    }
}
