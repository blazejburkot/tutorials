package lucene.searching;

import lucene.util.index.InMemoryIndexBuilder;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Field;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiSearcher;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopDocs;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class MultiIndexSearcherTest {
    private MultiSearcher multiSearcher;

    @Before
    public void setUp() throws Exception {
        final String[] animals = {"aardvark", "beaver", "coati", "dog", "elephant", "frog", "gila monster", "horse", "iguana", "javelina",
                "kangaroo", "lemur", "moose", "nematode", "orca", "python", "quokka", "rat", "scorpion", "tarantula", "uromastyx", "vicuna",
                "walrus", "xiphias", "yak", "zebra"};

        final InMemoryIndexBuilder indexBuilder1 = IndexBuilder.inMemoryBuilder();
        final InMemoryIndexBuilder indexBuilder2 = IndexBuilder.inMemoryBuilder();

        for (String animal : animals) {
            InMemoryIndexBuilder selectedBuilder = animal.charAt(0) < 'n' ? indexBuilder1 : indexBuilder2;
            selectedBuilder.withDocument().withField("animal", animal, Field.Store.YES, Field.Index.NOT_ANALYZED);
        }

        final IndexSearcher indexSearcher1 = new IndexSearcher(indexBuilder1.build());
        final IndexSearcher indexSearcher2 = new IndexSearcher(indexBuilder2.build());
        multiSearcher = new MultiSearcher(indexSearcher1, indexSearcher2);
    }

    @After
    public void cleanUp() throws IOException {
        multiSearcher.close();
    }

    @Test
    public void testMulti() throws Exception {
        final TermRangeQuery query = new TermRangeQuery("animal", "h", "t", true, true);

        final TopDocs hits = multiSearcher.search(query, 10);
        assertThat(hits.totalHits).isEqualTo(12);
    }
}
