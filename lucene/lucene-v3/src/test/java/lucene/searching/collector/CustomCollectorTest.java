package lucene.searching.collector;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomCollectorTest {
    private Directory directory;
    private IndexSearcher indexSearcher;

    @Before
    public void setUp() throws IOException {
        directory = IndexBuilder.withBooksData().build();
        indexSearcher = new IndexSearcher(directory);
    }

    @After
    public void cleanUp() throws IOException {
        indexSearcher.close();
        directory.close();
    }

    @Test
    public void testBookLinkCollector() throws Exception {
        final TermQuery query = new TermQuery(new Term("contents", "junit"));
        final BookLinkCollector collector = new BookLinkCollector();

        indexSearcher.search(query, collector);
        final Map<String, String> documents = collector.getDocuments();

        assertThat(documents).hasSize(indexSearcher.search(query, 10).totalHits);
        assertThat(documents.get("http://www.manning.com/loughran")).isEqualTo("ant in action");
    }

    @Test
    public void testAllDocCollector() throws Exception {
        final TermQuery query = new TermQuery(new Term("contents", "junit"));
        final AllDocCollector  collector = new AllDocCollector();

        indexSearcher.search(query, collector);
        final List<ScoreDoc> hits = collector.getHits();

        assertThat(hits).hasSize(indexSearcher.search(query, 10).totalHits);
    }
}
