package lucene.searching.collector;

import junit.framework.TestCase;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TimeLimitingCollector;
import org.apache.lucene.search.TimeLimitingCollector.TimeExceededException;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;

import static org.assertj.core.api.Assertions.assertThat;

public class TimeLimitingCollectorTest extends TestCase {
    public void testTimeLimitingCollector() throws Exception {
        final Directory directory = IndexBuilder.withBooksData().build();
        final IndexSearcher searcher = new IndexSearcher(directory);
        final Query query = new MatchAllDocsQuery();
        int numAllBooks = searcher.search(query, 10).totalHits;

        try {
            final TopScoreDocCollector topDocs = TopScoreDocCollector.create(10, false);
            final Collector collector = new TimeLimitingCollector(topDocs, 1);

            searcher.search(query, collector);

            assertThat(topDocs.getTotalHits()).isEqualTo(numAllBooks);
        } catch (TimeExceededException tee) {
            System.out.println("Too much time taken.");
        }

        searcher.close();
        directory.close();
    }
}
