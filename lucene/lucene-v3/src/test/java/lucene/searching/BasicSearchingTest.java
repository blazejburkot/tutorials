package lucene.searching;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class BasicSearchingTest {

    private static Directory directory;
    private static IndexSearcher searcher;

    @BeforeClass
    public static void setUpOnce() throws IOException {
        directory = IndexBuilder.withBooksData().build();
        searcher = new IndexSearcher(directory);
    }

    @AfterClass
    public static void cleanUpOnce() throws IOException {
        directory.close();
        searcher.close();
    }

    @Test
    public void testTerm() throws Exception {
        final Term t = new Term("subject", "ant");
        final Query query = new TermQuery(t);
        final TopDocs docs = searcher.search(query, 10);
        assertThat(docs.totalHits).isEqualTo(1);
    }

    @Test
    public void testQueryParser() throws Exception {
        final QueryParser parser = new QueryParser(Version.LUCENE_30, "contents", new SimpleAnalyzer());

        final Query query = parser.parse("+JUNIT +ANT -MOCK");
        final TopDocs docs = searcher.search(query, 10);
        final Document d = searcher.doc(docs.scoreDocs[0].doc);

        assertThat(docs.totalHits).isEqualTo(1);
        assertThat(d.get("title")).isEqualTo("Ant in Action");
    }

    @Test
    public void testQueryParser2() throws Exception {
        final QueryParser parser = new QueryParser(Version.LUCENE_30, "contents", new SimpleAnalyzer());

        final Query query = parser.parse("mock OR junit");
        final TopDocs docs = searcher.search(query, 10);
        assertThat(docs.totalHits).isEqualTo(2);
    }
}
