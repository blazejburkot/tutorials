package lucene.searching;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class NearRealTimeTest {

    @Test
    public void testNearRealTime() throws Exception {
        final Directory dir = new RAMDirectory();
        final IndexWriter indexWriter = new IndexWriter(dir, new StandardAnalyzer(Version.LUCENE_30), IndexWriter.MaxFieldLength.UNLIMITED);
        for (int idx = 0; idx < 10; idx++) {
            addDocument(indexWriter, idx, "aaa");
        }

        // IndexWriter returns a reader that's able to search
        // all previously committed changes to the index,
        // plus any uncommitted changes.  The returned reader is always readOnly.
        final IndexReader reader = indexWriter.getReader();
        final Query query = new TermQuery(new Term("text", "aaa"));

        final IndexSearcher searcher = new IndexSearcher(reader);
        final TopDocs hitsBeforeDelete = searcher.search(query, 1);
        assertThat(hitsBeforeDelete.totalHits).isEqualTo(10);

        indexWriter.deleteDocuments(new Term("id", "7"));

        final TopDocs hitsAfterDelete = searcher.search(query, 10);
        assertThat(hitsAfterDelete.totalHits).isEqualTo(10);

        final IndexReader newReader = reader.reopen();
        final IndexSearcher newSearcher = new IndexSearcher(newReader);
        assertThat(newReader).isNotEqualTo(reader);
        reader.close();

        // The changes made with the writer are reflected in new search.
        final TopDocs hitsFromNewReader = newSearcher.search(query, 10);
        assertThat(hitsFromNewReader.totalHits).isEqualTo(9);

        newReader.close();
        indexWriter.close();
    }

    private void addDocument(final IndexWriter indexWriter, final int id, final String text) throws IOException {
        Document doc = new Document();
        doc.add(new Field("id", Integer.toString(id), Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS));
        doc.add(new Field("text", text, Field.Store.NO, Field.Index.ANALYZED));

        indexWriter.addDocument(doc);
    }
}
