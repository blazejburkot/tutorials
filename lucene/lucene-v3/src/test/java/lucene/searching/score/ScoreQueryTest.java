package lucene.searching.score;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.function.CustomScoreProvider;
import org.apache.lucene.search.function.CustomScoreQuery;
import org.apache.lucene.search.function.FieldScoreQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ScoreQueryTest {
    private StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
    private IndexSearcher indexSearcher;
    private Directory directory;

    @Before
    public void setUp() throws Exception {
        directory = IndexBuilder.inMemoryBuilder()
                .withDocument()
                .withField("score", "7", Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                .withField("content", "this hat is green", Field.Store.NO, Field.Index.ANALYZED)
                .withDocument()
                .withField("score", "42", Field.Store.NO, Field.Index.NOT_ANALYZED_NO_NORMS)
                .withField("content", "this hat is blue", Field.Store.NO, Field.Index.ANALYZED)
                .withAnalyzer(analyzer)
                .build();

        indexSearcher = new IndexSearcher(directory, true);
    }

    @After
    public void tearDown() throws Exception {
        indexSearcher.close();
        directory.close();
    }

    @Test
    public void testFieldScoreQuery() throws Throwable {
        final Query query = new FieldScoreQuery("score", FieldScoreQuery.Type.BYTE);
        final TopDocs hits = indexSearcher.search(query, 10);

        assertThat(hits.totalHits).isEqualTo(2);
        assertThat(hits.scoreDocs[0])
                .hasFieldOrPropertyWithValue("doc", 1)
                .hasFieldOrPropertyWithValue("score", 42.0f);
        assertThat(hits.scoreDocs[1])
                .hasFieldOrPropertyWithValue("doc", 0)
                .hasFieldOrPropertyWithValue("score", 7.0f);
    }

    @Test
    public void testCustomScoreQuery() throws Throwable {
        final QueryParser queryParser = new QueryParser(Version.LUCENE_30, "content", analyzer);
        final Query query = queryParser.parse("the green hat");
        final FieldScoreQuery fieldScoreQuery = new FieldScoreQuery("score", FieldScoreQuery.Type.BYTE);
        final CustomScoreQuery customScoreQuery = new CustomScoreQuery(query, fieldScoreQuery) {
            public CustomScoreProvider getCustomScoreProvider(IndexReader r) {
                return new CustomScoreProvider(r) {
                    public float customScore(int doc, float subQueryScore, float valSrcScore) {
                        return (float) (Math.sqrt(subQueryScore) * valSrcScore);
                    }
                };
            }
        };

        final TopDocs hits = indexSearcher.search(customScoreQuery, 10);
        assertThat(hits.totalHits).isEqualTo(2);
        assertThat(hits.scoreDocs[0].doc).isEqualTo(1);
        assertThat(hits.scoreDocs[1].doc).isEqualTo(0);
    }

    @Test
    public void testRecency() throws Throwable {
        final Directory dir = IndexBuilder.withBooksData().withAnalyzer(analyzer).build();
        final IndexReader indexReader = IndexReader.open(dir);
        final IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        indexSearcher.setDefaultFieldSortScoring(true, true);

        final QueryParser parser = new QueryParser(Version.LUCENE_30, "contents", analyzer);
        final Query query = parser.parse("java in action");       // #A
        final Query recencyBoostingQuery = new RecencyBoostingQuery(query, 2.0, 100 * 365, "pubmonthAsDay");
        final Sort sort = new Sort(SortField.FIELD_SCORE, new SortField("title2", SortField.STRING));
        final TopDocs hits = indexSearcher.search(recencyBoostingQuery, null, 5, sort);

        for (int i = 0; i < hits.scoreDocs.length; i++) {
            final Document doc = indexReader.document(hits.scoreDocs[i].doc);
            final float score = hits.scoreDocs[i].score;
            System.out.println(String.format("%d: [pubmonth=%s] [score=%s] %s ", i + 1, doc.get("pubmonth"), score, doc.get("title")));
        }

        indexSearcher.close();
        indexReader.close();
        dir.close();
    }
}
