package lucene.searching.sort;

import lucene.util.index.InMemoryIndexBuilder;
import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FieldDoc;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.store.Directory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class DistanceSortingTest {
    private IndexSearcher searcher;
    private Query query;

    @Before
    public void setUp() throws Exception {
        final InMemoryIndexBuilder indexBuilder = IndexBuilder.inMemoryBuilder();
        addPoint(indexBuilder, "El Charro", "restaurant", 1, 2);
        addPoint(indexBuilder, "Cafe Poca Cosa", "restaurant", 5, 9);
        addPoint(indexBuilder, "Los Betos", "restaurant", 9, 6);
        addPoint(indexBuilder, "Nico's Taco Shop", "restaurant", 3, 8);

        final Directory directory = indexBuilder.build();
        searcher = new IndexSearcher(directory);
        query = new TermQuery(new Term("type", "restaurant"));
    }

    private void addPoint(InMemoryIndexBuilder indexBuilder, String name, String type, int x, int y) {
        indexBuilder.withDocument()
                .withField("name", name, Field.Store.YES, Field.Index.NOT_ANALYZED)
                .withField("type", type, Field.Store.YES, Field.Index.NOT_ANALYZED)
                .withField("x", Integer.toString(x), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS)
                .withField("y", Integer.toString(y), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS);
    }

    @After
    public void cleanUp() throws IOException {
        searcher.close();
    }

    @Test
    public void testNearestRestaurantToHome() throws Exception {
        final Sort sort = new Sort(new SortField("unused", new DistanceComparatorSource(0, 0)));

        final TopDocs hits = searcher.search(query, null, 10, sort);

        assertThat(getNameField(hits, 0)).isEqualTo("El Charro");
        assertThat(getNameField(hits, 3)).isEqualTo("Los Betos");
    }


    @Test
    public void testNeareastRestaurantToWork() throws Exception {
        final Sort sort = new Sort(new SortField("unused", new DistanceComparatorSource(10, 10)));

        final int numberOfReturnedDocs = 3;
        final TopFieldDocs hits = searcher.search(query, null, numberOfReturnedDocs, sort);
        final FieldDoc fieldDoc = (FieldDoc) hits.scoreDocs[0];

        assertThat(hits.totalHits).isEqualTo(4);
        assertThat(hits.scoreDocs.length).isEqualTo(numberOfReturnedDocs);
        assertThat((float) fieldDoc.fields[0]).isEqualTo((float) Math.sqrt(17));
        assertThat(getNameField(hits, 0)).isEqualTo("Los Betos");

        dumpDocs(sort, hits);
    }


    private void dumpDocs(Sort sort, TopFieldDocs docs) throws IOException {
        System.out.println("Sorted by: " + sort);
        final ScoreDoc[] scoreDocs = docs.scoreDocs;
        for (final ScoreDoc scoreDoc : scoreDocs) {
            float distance = (float) ((FieldDoc) scoreDoc).fields[0];
            final Document doc = searcher.doc(scoreDoc.doc);
            System.out.println(String.format("   %s at (%s, %s) -> %s", doc.get("name"),  doc.get("x"), doc.get("y"), distance));
        }
    }

    private String getNameField(final TopDocs hits, final int idx) throws IOException {
        return searcher.doc(hits.scoreDocs[idx].doc).get("name");
    }
}
