package lucene.analysis.payload;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

import java.io.Reader;

public class BulletinPayloadsAnalyzer extends Analyzer {
    private final StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
    private boolean isBulletin;
    private float boost;

    BulletinPayloadsAnalyzer(float boost) {
        this.boost = boost;
    }

    void setIsBulletin(boolean v) {
        isBulletin = v;
    }

    public TokenStream tokenStream(String fieldName, Reader reader) {
        final TokenStream tokenStream = analyzer.tokenStream(fieldName, reader);
        BulletinPayloadsFilter stream = new BulletinPayloadsFilter(tokenStream, boost);
        stream.setIsBulletin(isBulletin);
        return stream;
    }
}
