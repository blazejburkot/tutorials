package lucene.analysis.util;

import com.google.common.collect.Lists;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;

public class AnalyzerUtils {

    public static void printTerms(final Analyzer analyzer, final String text) {
        printTerms(analyzer, text, false);
    }

    public static void printTerms(final Analyzer analyzer, final String text, final boolean printPosition) {
        System.out.print("analyzer: " + analyzer.getClass().getSimpleName() + ", text: " + text);

        final List<AnalysisToken> tokens = processText(analyzer, text);
        int lastPosition = -1;
        for (AnalysisToken token : tokens) {
            if (lastPosition != token.getPosition()) {
                lastPosition = token.getPosition();
                if (printPosition) {
                    System.out.print(String.format("\n  %d: ", lastPosition));
                }
            }
            System.out.print( String.format("[%s] ", token.getTerm()));
        }
        System.out.println();
    }

    public static List<AnalysisToken> processText(final Analyzer analyzer, final String textValue) {
        final TokenStream tokenStream = analyzer.tokenStream("contents",  new StringReader(textValue));

        final TermAttribute termAttribute = tokenStream.addAttribute(TermAttribute.class);
        final PositionIncrementAttribute positionIncrementAttribute = tokenStream.addAttribute(PositionIncrementAttribute.class);
        final OffsetAttribute offsetAttribute = tokenStream.addAttribute(OffsetAttribute.class);
        final TypeAttribute typeAttribute = tokenStream.addAttribute(TypeAttribute.class);

        int position = 0;
        final List<AnalysisToken> analysisTokenList = new ArrayList<>();
        try {
            while (tokenStream.incrementToken()) {
                position = position + positionIncrementAttribute.getPositionIncrement();

                final AnalysisToken analysisToken = AnalysisToken.builder()
                        .position(position)
                        .term(termAttribute.term())
                        .startOffset(offsetAttribute.startOffset())
                        .endOffset(offsetAttribute.endOffset())
                        .type(typeAttribute.type())
                        .build();

                analysisTokenList.add(analysisToken);
            }
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }

        return analysisTokenList;
    }

    public static String markSpanTerm(final Analyzer analyzer, final String fieldValue,
                                      final int spanStart, final int spanEnd) {
        final List<String> terms = Lists.transform(processText(analyzer, fieldValue), AnalysisToken::getTerm);
        final StringBuilder stringBuilder = new StringBuilder();
        for (int idx = 0; idx < terms.size(); ++idx) {
            stringBuilder.append(idx == spanStart ? "<" : "")
                    .append(terms.get(idx))
                    .append(idx + 1 == spanEnd ? ">" : "")
                    .append(" ");
        }
        return stringBuilder.toString();
    }
}
