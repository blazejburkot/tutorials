package lucene.analysis.util;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderClassName = "Builder")
public class AnalysisToken {
    private String term;
    private int position;
    private int startOffset;
    private int endOffset;
    private String type;

}
