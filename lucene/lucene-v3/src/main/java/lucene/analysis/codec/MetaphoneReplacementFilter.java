package lucene.analysis.codec;

import org.apache.commons.codec.language.Metaphone;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;

import java.io.IOException;

public class MetaphoneReplacementFilter extends TokenFilter {
    private static final String METAPHONE = "metaphone";

    private Metaphone metaphoner = new Metaphone();
    private TermAttribute termAttr;
    private TypeAttribute typeAttr;

    public MetaphoneReplacementFilter(final TokenStream input) {
        super(input);
        termAttr = addAttribute(TermAttribute.class);
        typeAttr = addAttribute(TypeAttribute.class);
    }

    public boolean incrementToken() throws IOException {
        if (!input.incrementToken()) {
            return false;
        }

        final String encoded = metaphoner.encode(termAttr.term());  // Convert to Metaphone encoding
        termAttr.setTermBuffer(encoded); // Overwrite with encoded text
        typeAttr.setType(METAPHONE);
        return true;
    }
}
