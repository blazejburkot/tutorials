package lucene.analysis;

import lucene.analysis.util.AnalyzerUtils;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.KeywordAnalyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.util.Version;

import java.util.Arrays;
import java.util.List;

public class TokenizersComparison {
    private static final String[] TEXTS = {
            "The quick brown fox jumped over the lazy dog",
            "XY&Z Corporation - xyz@example.com",
            "No Fluff, Just Stuff",
    };

    private static List<Analyzer> analyzers = Arrays.asList(
            new WhitespaceAnalyzer(),
            new SimpleAnalyzer(),
            new StopAnalyzer(Version.LUCENE_30),
            new KeywordAnalyzer(),
            new StandardAnalyzer(Version.LUCENE_30)
    );


    public static void main(final String[] args) {
        for (String text : TEXTS) {
            System.out.println("\nAnalyzing \"" + text + "\"");
            analyzers.forEach(analyzer -> AnalyzerUtils.printTerms(analyzer, text));
        }
    }
}
