package lucene.analysis.synonym;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.util.AttributeSource;

import java.io.IOException;
import java.util.List;
import java.util.Stack;

public class SynonymFilter extends TokenFilter {
    private final Stack<String> synonymStack;
    private final SynonymEngine engine;

    private final TermAttribute termAtt;
    private final PositionIncrementAttribute posIncrAtt;

    private AttributeSource.State current;

    public SynonymFilter(TokenStream in, SynonymEngine engine) {
        super(in);
        this.synonymStack = new Stack<>();
        this.engine = engine;

        this.termAtt = addAttribute(TermAttribute.class);
        this.posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    }

    public boolean incrementToken() throws IOException {
        if (synonymStack.size() > 0) {
            String synonym = synonymStack.pop();
            restoreState(current);
            termAtt.setTermBuffer(synonym);
            posIncrAtt.setPositionIncrement(0);
            return true;
        }

        if (!input.incrementToken())
            return false;

        if (addAliasesToStack()) {
            current = captureState(); // Save current token
        }

        return true; // Return current token
    }

    private boolean addAliasesToStack() throws IOException {
        final List<String> synonyms = engine.getSynonyms(termAtt.term());
        if (synonyms == null || synonyms.isEmpty()) {
            return false;
        }
        synonyms.forEach(synonymStack::push);
        return true;
    }
}
