package lucene.analysis.synonym;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

import java.io.Reader;

public class SynonymAnalyzer extends Analyzer {
    private SynonymEngine engine;

    public SynonymAnalyzer(SynonymEngine engine) {
        this.engine = engine;
    }

    public TokenStream tokenStream(String fieldName, Reader reader) {
        final LowerCaseFilter filterChain = new LowerCaseFilter(new StandardFilter(new StandardTokenizer(Version.LUCENE_30, reader)));
        final StopFilter stopFilter = new StopFilter(true, filterChain, StopAnalyzer.ENGLISH_STOP_WORDS_SET);
        return new SynonymFilter(stopFilter, engine );
    }
}
