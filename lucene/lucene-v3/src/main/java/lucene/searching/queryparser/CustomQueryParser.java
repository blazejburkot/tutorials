package lucene.searching.queryparser;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.spans.SpanNearQuery;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.util.Version;

import java.util.stream.Stream;

public class CustomQueryParser extends QueryParser {

    public CustomQueryParser(Version matchVersion, String field, Analyzer analyzer) {
        super(matchVersion, field, analyzer);
    }

    @Override
    protected final Query getWildcardQuery(String field, String termStr) throws ParseException {
        throw new ParseException("Wildcard not allowed");
    }

    @Override
    protected Query getFuzzyQuery(String field, String term, float minSimilarity) throws ParseException {
        throw new ParseException("Fuzzy queries not allowed");
    }

    /**
     * Replace PhraseQuery with SpanNearQuery to force in-order
     * phrase matching rather than reverse.
     */
    @Override
    protected Query getFieldQuery(String field, String queryText, int slop) throws ParseException {
        final Query originalQuery = super.getFieldQuery(field, queryText, slop);
        if (!(originalQuery instanceof PhraseQuery)) {
            return originalQuery;
        }

        final SpanTermQuery[] clauses = Stream.of(((PhraseQuery) originalQuery).getTerms())
                .map(SpanTermQuery::new)
                .toArray(SpanTermQuery[]::new);
        return new SpanNearQuery(clauses, slop, true);
    }
}
