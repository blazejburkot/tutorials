package lucene.searching.collector;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.FieldCache;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BookLinkCollector extends Collector {
    private Map<String, String> documents = new HashMap<>();
    private Scorer scorer;
    private String[] urls;
    private String[] titles;

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void setScorer(Scorer scorer) {
        this.scorer = scorer;
    }

    @Override
    public void setNextReader(IndexReader reader, int docBase) throws IOException {
        urls = FieldCache.DEFAULT.getStrings(reader, "url");
        titles = FieldCache.DEFAULT.getStrings(reader, "title2");
    }

    @Override
    public void collect(int docID) {
        try {
            final String url = urls[docID];
            final String title = titles[docID];
            documents.put(url, title);
            System.out.println(title + ":" + scorer.score());
        } catch (IOException e) {
            // ignore
        }
    }

    public Map<String, String> getDocuments() {
        return Collections.unmodifiableMap(documents);
    }
}
