package lucene.searching.collector;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Scorer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AllDocCollector extends Collector {
    private List<ScoreDoc> docs = new ArrayList<>();
    private Scorer scorer;
    private int docBase;

    @Override
    public boolean acceptsDocsOutOfOrder() {
        return true;
    }

    @Override
    public void setScorer(Scorer scorer) {
        this.scorer = scorer;
    }

    @Override
    public void setNextReader(IndexReader reader, int docBase) {
        this.docBase = docBase;
    }

    @Override
    public void collect(int doc) throws IOException {
        docs.add(new ScoreDoc(doc + docBase, scorer.score()));
    }

    public void reset() {
        docs.clear();
    }

    public List<ScoreDoc> getHits() {
        return docs;
    }
}
