package lucene.searching;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Explanation;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

public class Explainer {
    public static void main(String[] args) throws Exception {
        String queryExpression = "junit";

        Directory directory = IndexBuilder.withBooksData().build();
        QueryParser parser = new QueryParser(Version.LUCENE_30, "contents", new SimpleAnalyzer());
        Query query = parser.parse(queryExpression);

        System.out.println("Query: " + queryExpression);

        IndexSearcher searcher = new IndexSearcher(directory);
        TopDocs topDocs = searcher.search(query, 10);

        for (ScoreDoc match : topDocs.scoreDocs) {
            Explanation explanation = searcher.explain(query, match.doc);

            Document doc = searcher.doc(match.doc);
            System.out.println("-- " + doc.get("title"));
            System.out.println(explanation.toString());
//            System.out.println(explanation.toHtml());
        }
        searcher.close();
        directory.close();
    }
}
