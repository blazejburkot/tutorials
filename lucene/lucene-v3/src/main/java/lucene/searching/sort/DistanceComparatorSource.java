package lucene.searching.sort;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.FieldCache;
import org.apache.lucene.search.FieldComparator;
import org.apache.lucene.search.FieldComparatorSource;

import java.io.IOException;

public class DistanceComparatorSource extends FieldComparatorSource {
    private int x;
    private int y;

    public DistanceComparatorSource(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public FieldComparator newComparator(String fieldName, int numHits, int sortPos, boolean reversed) {
        return new DistanceScoreDocLookupComparator(numHits);
    }

    private class DistanceScoreDocLookupComparator extends FieldComparator {
        private int[] xDoc, yDoc;
        private float[] values;
        private float bottom;

        DistanceScoreDocLookupComparator(final int numHits) {
            this.values = new float[numHits];
        }

        @Override
        public void setNextReader(IndexReader reader, int docBase) throws IOException {
            xDoc = FieldCache.DEFAULT.getInts(reader, "x");
            yDoc = FieldCache.DEFAULT.getInts(reader, "y");
        }

        @Override
        public int compare(int slot1, int slot2) {
            return Float.compare(values[slot1], values[slot2]);
        }

        @Override
        public void setBottom(int slot) {
            bottom = values[slot];
        }

        @Override
        public int compareBottom(int doc) {
            return Float.compare(bottom, getDistance(doc));
        }

        @Override
        public void copy(int slot, int doc) {
            values[slot] = getDistance(doc);
        }

        @Override
        public Comparable value(int slot) {
            return values[slot];
        }

        private float getDistance(int doc) {
            int deltax = xDoc[doc] - x;
            int deltay = yDoc[doc] - y;
            return (float) Math.sqrt(deltax * deltax + deltay * deltay);
        }
    }

    @Override
    public String toString() {
        return "Distance from (" + x + "," + y + ")";
    }
}

