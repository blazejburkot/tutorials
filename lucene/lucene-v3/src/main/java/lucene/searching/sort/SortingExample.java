package lucene.searching.sort;

import lucene.util.index.IndexBuilder;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.text.DecimalFormat;

public class SortingExample {
    private static DecimalFormat scoreFormatter = new DecimalFormat("0.#####");

    private static Directory directory;

    public static void displayResults(Query query, Sort sort) throws IOException {
        IndexSearcher searcher = new IndexSearcher(directory);

        // We ask IndexSearcher (to compute scores per hit.
        // Then we call the overloaded search method that accepts the custom Sort
        searcher.setDefaultFieldSortScoring(true, false);

        TopDocs results = searcher.search(query, null, 20, sort);

        System.out.println("\nResults for: " + query.toString() + " sorted by " + sort);
        System.out.println(
                StringUtils.rightPad("Title", 30) +
                        StringUtils.rightPad("pubmonth", 10) +
                        StringUtils.center("id", 4) +
                        StringUtils.center("score", 15) +
                        StringUtils.repeat(" ", 2) + "category"
        );


        for (ScoreDoc scoreDoc : results.scoreDocs) {
            int docID = scoreDoc.doc;
            Document doc = searcher.doc(docID);
            System.out.println(
                    StringUtils.rightPad(StringUtils.abbreviate(doc.get("title"), 29), 30) +
                            StringUtils.rightPad(doc.get("pubmonth"), 10) +
                            StringUtils.center("" + docID, 4) +
                            StringUtils.leftPad(scoreFormatter.format(scoreDoc.score), 12) +
                            StringUtils.repeat(" ", 5) + doc.get("category")

            );
//            System.out.println(searcher.explain(query, docID));   // #7
        }

        searcher.close();
    }

    public static void main(String[] args) throws Exception {
        QueryParser parser = new QueryParser(Version.LUCENE_30, "contents", new StandardAnalyzer(Version.LUCENE_30));
        Query allBooks = new MatchAllDocsQuery();
        BooleanQuery query = new BooleanQuery();
        query.add(allBooks, BooleanClause.Occur.SHOULD);
        query.add(parser.parse("java OR action"), BooleanClause.Occur.SHOULD);

        directory = IndexBuilder.withBooksData().build();

        displayResults(query, Sort.RELEVANCE);
        displayResults(query, Sort.INDEXORDER);
        displayResults(query, new Sort(new SortField("category", SortField.STRING)));
        displayResults(query, new Sort(new SortField("pubmonth", SortField.INT, true)));

        final Sort multifieldSort = new Sort(
                new SortField("category", SortField.STRING),
                SortField.FIELD_SCORE,
                new SortField("pubmonth", SortField.INT, true));
        displayResults(query, multifieldSort);

        displayResults(query, new Sort(SortField.FIELD_SCORE, new SortField("category", SortField.STRING)));
        directory.close();
    }
}