package lucene.searching.score;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.FieldCache;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.function.CustomScoreProvider;
import org.apache.lucene.search.function.CustomScoreQuery;

import java.io.IOException;
import java.util.Date;

class RecencyBoostingQuery extends CustomScoreQuery {
    private static final int MSEC_PER_DAY = 1000 * 3600 * 24;

    private double multiplier;
    private int today;
    private int maxDaysAgo;
    private String dayField;

    public RecencyBoostingQuery(final Query query, final double multiplier, final int maxDaysAgo, final String dayField) {
        super(query);
        this.today = (int) (new Date().getTime() / MSEC_PER_DAY);
        this.multiplier = multiplier;
        this.maxDaysAgo = maxDaysAgo;
        this.dayField = dayField;
    }

    @Override
    public CustomScoreProvider getCustomScoreProvider(final IndexReader indexReader) throws IOException {
        return new RecencyBooster(indexReader);
    }

    private class RecencyBooster extends CustomScoreProvider {
        private final int[] publishDay;

        RecencyBooster(final IndexReader indexReader) throws IOException {
            super(indexReader);
            publishDay = FieldCache.DEFAULT.getInts(indexReader, dayField); // Retrieve days from field cache
        }

        @Override
        public float customScore(final int doc, final float subQueryScore, final float valSrcScore) {
            int daysAgo = today - publishDay[doc];
            if (daysAgo < maxDaysAgo) {
                float boost = (float) (multiplier * (maxDaysAgo - daysAgo) / maxDaysAgo);
                return (float) (subQueryScore * (1.0 + boost));
            } else {
                return subQueryScore;
            }
        }
    }
}
