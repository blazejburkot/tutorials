package lucene.searching;

import lucene.util.index.IndexBuilder;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similar.MoreLikeThis;
import org.apache.lucene.store.Directory;

public class BooksMoreLikeThis {

    public static void main(final String[] args) throws Throwable {
        final Directory directory = IndexBuilder.withBooksData().build();
        final IndexReader reader = IndexReader.open(directory);
        final IndexSearcher searcher = new IndexSearcher(reader);

        final int numDocs = reader.maxDoc();

        final MoreLikeThis mlt = new MoreLikeThis(reader);
        mlt.setFieldNames(new String[]{"title", "author"});
        mlt.setMinTermFreq(1);
        mlt.setMinDocFreq(1);

        for (int docID = 0; docID < numDocs; docID++) {
            final Document doc = reader.document(docID);
            System.out.println("\n" + doc.get("title"));

            final Query query = mlt.like(docID);
            System.out.println("  query=" + query + "\n  results:");

            final TopDocs similarDocs = searcher.search(query, 10);
            if (similarDocs.totalHits == 0) {
                System.out.println("  None like this");
            }
            for (int i = 0; i < similarDocs.scoreDocs.length; i++) {
                if (similarDocs.scoreDocs[i].doc != docID) {
                    final Document similarDoc = reader.document(similarDocs.scoreDocs[i].doc);
                    System.out.println("  -> " + similarDoc.getField("title").stringValue());
                }
            }
        }

        searcher.close();
        reader.close();
        directory.close();
    }
}