package lucene.util.index;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import java.util.Collection;

public abstract class IndexBuilder {
    protected Directory directory;
    private Analyzer analyzer;
    private IndexWriter.MaxFieldLength maxFieldLength;

    protected IndexBuilder() {
        this.directory = new RAMDirectory();
        this.analyzer = new WhitespaceAnalyzer();
        this.maxFieldLength = IndexWriter.MaxFieldLength.UNLIMITED;
    }

    public static IndexBuilder builderWithSampleData() {
        return FileBasedIndexBuilder.sampleData();
    }

    public static IndexBuilder withBooksData() {
        return FileBasedIndexBuilder.booksData();
    }

    public static InMemoryIndexBuilder inMemoryBuilder() {
        return new InMemoryIndexBuilder();
    }

    public IndexBuilder withAnalyzer(final Analyzer newAnalyzer) {
        this.analyzer = newAnalyzer;
        return this;
    }

    public Directory build() {
        try {
            final IndexWriter indexWriter = new IndexWriter(directory, analyzer, maxFieldLength);

            final Collection<Document> documents = getDocuments();
            for (Document document : documents) {
                indexWriter.addDocument(document);
            }

            indexWriter.close();
            return directory;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract Collection<Document> getDocuments() throws Exception;
}
