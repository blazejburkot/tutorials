package lucene.util.index;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.ArrayList;
import java.util.List;

public class InMemoryIndexBuilder extends IndexBuilder {
    private List<Document> documents = new ArrayList<>();
    private Document document;

    InMemoryIndexBuilder() {
    }

    public InMemoryIndexBuilder withDocument() {
        document = new Document();
        documents.add(document);
        return this;
    }

    public InMemoryIndexBuilder withField(final String name, final String value, final Field.Store store, final Field.Index index) {
        document.add(new Field(name, value, store, index));
        return this;
    }

    @Override
    protected final List<Document> getDocuments() {
        return documents;
    }
}
