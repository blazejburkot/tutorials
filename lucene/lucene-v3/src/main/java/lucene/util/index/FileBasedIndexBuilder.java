package lucene.util.index;

import lucene.util.BookDocumentReader;
import lucene.util.IOUtils;
import lucene.util.SampleDocumentReader;

import org.apache.lucene.document.Document;

import java.io.File;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

class FileBasedIndexBuilder extends IndexBuilder {
    private String textFilesRootDirectoryPath;
    private Function<File, Document> documentReader;

    private FileBasedIndexBuilder() {
    }

    static FileBasedIndexBuilder sampleData() {
        final FileBasedIndexBuilder builder = new FileBasedIndexBuilder();
        builder.textFilesRootDirectoryPath = IOUtils.getResourceAbsolutePath("/data/sample");
        builder.documentReader = new SampleDocumentReader();
        return builder;
    }

    static FileBasedIndexBuilder booksData() {
        final FileBasedIndexBuilder builder = new FileBasedIndexBuilder();
        builder.textFilesRootDirectoryPath = IOUtils.getResourceAbsolutePath("/data/books");
        builder.documentReader = new BookDocumentReader();
        return builder;
    }

    protected List<Document> getDocuments() {
        return IOUtils.readTextFiles(textFilesRootDirectoryPath)
                .map(documentReader)
                .collect(Collectors.toList());
    }
}
