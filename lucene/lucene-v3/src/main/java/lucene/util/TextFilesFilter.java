package lucene.util;

import java.io.File;
import java.io.FileFilter;

public class TextFilesFilter implements FileFilter {
    public boolean accept(File path) {
        final String name = path.getName().toLowerCase();
        return name.endsWith(".txt") || name.endsWith(".properties");
    }
}
