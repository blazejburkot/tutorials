package lucene.util;

import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericField;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.Properties;
import java.util.function.Function;

public class BookDocumentReader implements Function<File, Document> {

    @Override
    public Document apply(final File file) {
        try {
            final Properties props = new Properties();
            props.load(new FileInputStream(file));

            final Document doc = new Document();

            // category comes from relative path below the base directory
            final String fileParent = file.getParent();
            final String category = fileParent.substring(fileParent.indexOf("books") + 5).replace(File.separatorChar, '/');

            final String isbn = props.getProperty("isbn");
            final String title = props.getProperty("title");
            final String author = props.getProperty("author");
            final String url = props.getProperty("url");
            final String subject = props.getProperty("subject");
            final String pubmonth = props.getProperty("pubmonth");

//            System.out.println(title + "\n" + author + "\n" + subject + "\n" + pubmonth + "\n" + category + "\n---------");

            doc.add(new Field("isbn", isbn, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.add(new Field("category", category, Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.add(new Field("title", title, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));
            doc.add(new Field("title2", title.toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS, Field.TermVector.WITH_POSITIONS_OFFSETS));

            // split multiple authors into unique field instances
            final String[] authors = author.split(",");
            for (String a : authors) {
                doc.add(new Field("author", a, Field.Store.YES, Field.Index.NOT_ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));
            }

            doc.add(new Field("url", url, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
            doc.add(new Field("subject", subject, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));

            doc.add(new NumericField("pubmonth", Field.Store.YES, true).setIntValue(Integer.parseInt(pubmonth)));

            final Date pubmonthDate = DateTools.stringToDate(pubmonth);
            doc.add(new NumericField("pubmonthAsDay").setIntValue((int) (pubmonthDate.getTime() / (1000*3600*24))));


            for(String text : new String[] {title, subject, author, category}) {
                doc.add(new Field("contents", text, Field.Store.NO, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));
            }

            return doc;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
