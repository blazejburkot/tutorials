package lucene.util;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Function;

public class SampleDocumentReader implements Function<File, Document> {

    @Override
    public Document apply(final File file) {
        try {
            Document doc = new Document();
            doc.add(new Field("contents", new FileReader(file)));
            doc.add(new Field("filename", file.getName(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.add(new Field("fullpath", file.getCanonicalPath(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            return doc;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
