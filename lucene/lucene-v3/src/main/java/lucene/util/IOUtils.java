package lucene.util;

import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Log4j2
public class IOUtils {

    public static Stream<File> readTextFiles() {
        final String dataDir = IOUtils.getResourceAbsolutePath("/data/sample");
        return readTextFiles(dataDir);
    }

    public static Stream<File> readTextFiles(final String rootDirAbsolutePath) {
        try {
            final FileFilter filter = new TextFilesFilter();
            return Files.walk(Paths.get(rootDirAbsolutePath))
                    .map(Path::toFile)
                    .filter(IOUtils::isFileReadable)
                    .filter(filter::accept)
//                    .peek(System.out::println)
                    ;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
    private static boolean isFileReadable(final File file) {
        return !file.isDirectory() && !file.isHidden() && file.exists() && file.canRead();
    }

    /**
     * @param resource - must start with /
     * @return absolute path of resource
     */
    public static String getResourceAbsolutePath(final String resource) {
        try {
            return new File(IOUtils.class.getResource(resource).toURI()).getAbsolutePath();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param outputDirectoryName - eg. 'sample'
     * @return absolute path of output directory
     */
    public static String prepareOutputDirectory(final String outputDirectoryName) {
        final Path rootOutputDir = getRootOfRepository().resolve("lucene/lucene-v3/output");
        final Path outputDirectory = rootOutputDir.resolve(outputDirectoryName);

        final File dir = outputDirectory.toFile();
        if (dir.exists()) {
            Stream.of(dir.listFiles()).forEach(File::delete);
        } else {
            dir.mkdirs();
        }

        log.info("Output directory: {}", dir.getAbsolutePath());
        return dir.getAbsolutePath();
    }


    public static Path getRootOfRepository() {
        Path path = Paths.get("").toAbsolutePath();
        while (!path.endsWith("tutorials")) {
            if (path == path.getRoot()) {
                throw new IllegalStateException();
            }
            path = path.getParent();
        }
        return path;
    }
}
