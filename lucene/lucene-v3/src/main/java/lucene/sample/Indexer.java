package lucene.sample;

import lombok.extern.log4j.Log4j2;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;

@Log4j2
public class Indexer {
    private IndexWriter indexWriter;

    public Indexer(String indexDir) throws Exception{
        final Directory dir = FSDirectory.open(new File(indexDir));
        final StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
        final IndexWriter.MaxFieldLength maxFieldLength = IndexWriter.MaxFieldLength.UNLIMITED;
        indexWriter = new IndexWriter(dir, analyzer, true, maxFieldLength);
        indexWriter.setUseCompoundFile(false);
        indexWriter.setInfoStream(System.out);
    }

    public void close() throws IOException {
        indexWriter.close(); // Close IndexWriter
    }

    public void index(String dataDir, FileFilter filter) throws Exception {
        long start = System.currentTimeMillis();
        File[] files = new File(dataDir).listFiles();
        for (File file : files) {
            if (isFileReadableAndIsAcceptedByFilter(filter, file)) {
                log.info("Indexing {}", file.getCanonicalPath());
                Document doc = getDocument(file);
                indexWriter.addDocument(doc);
            }
        }

        long end = System.currentTimeMillis();
        log.info("Indexing of {} files took {} [ms]",indexWriter.numDocs(),  end - start);
    }

    private boolean isFileReadableAndIsAcceptedByFilter(final FileFilter filter, final File file) {
        return !file.isDirectory() && !file.isHidden()
                && file.exists() && file.canRead()
                && (filter == null || filter.accept(file));
    }

    private Document getDocument(File f) throws Exception {
        Document doc = new Document();
        doc.add(new Field("contents", new FileReader(f))); // Index file content
        doc.add(new Field("filename", f.getName(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
        doc.add(new Field("fullpath", f.getCanonicalPath(), Field.Store.YES, Field.Index.NOT_ANALYZED));
        return doc;
    }
}
