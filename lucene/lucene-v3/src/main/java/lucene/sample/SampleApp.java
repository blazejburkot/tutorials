package lucene.sample;

import lucene.util.IOUtils;
import lucene.util.TextFilesFilter;

public class SampleApp {

    public static void main(String[] args) throws Exception {
        String indexDir = IOUtils.prepareOutputDirectory("sample");
        String dataDir = IOUtils.getResourceAbsolutePath("/data/sample");

        Indexer indexer = new Indexer(indexDir);
        indexer.index(dataDir, new TextFilesFilter());
        indexer.close();

        System.out.println();

        Searcher.search(indexDir, "patent");
    }
}
