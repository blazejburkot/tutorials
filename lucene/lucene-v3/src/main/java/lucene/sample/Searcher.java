package lucene.sample;

import lombok.extern.log4j.Log4j2;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;

@Log4j2
public class Searcher {

    public static void search(String indexDir, String queryStr) throws IOException, ParseException {
        Directory dir = FSDirectory.open(new File(indexDir));
        IndexSearcher indexSearcher = new IndexSearcher(dir);   // Open index

        final StandardAnalyzer standardAnalyzer = new StandardAnalyzer(Version.LUCENE_30);
        QueryParser queryParser = new QueryParser(Version.LUCENE_30, "contents", standardAnalyzer);
        Query query = queryParser.parse(queryStr);
        long start = System.currentTimeMillis();
        TopDocs hits = indexSearcher.search(query, 10);
        long end = System.currentTimeMillis();

        log.info("Found {} document(s) (in {} milliseconds) that matched query '{}':",
                hits.totalHits, end - start, queryStr);

        for (ScoreDoc scoreDoc : hits.scoreDocs) {
            Document doc = indexSearcher.doc(scoreDoc.doc); // Retrieve matching document
            log.info("  score {}, path {}", scoreDoc.score, doc.get("fullpath"));
        }

        indexSearcher.close(); // Close IndexSearcher
    }
}
